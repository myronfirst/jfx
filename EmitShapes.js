import { Point, Rect, Circle, Line } from './Shapes.js'
import { RandomNum, RandomInt } from './Utilities.js'
import { CSManager } from './CSManager.js'

export class PointEmitShape {
  constructor (x, y) {
    this.point = new Point(x, y)
  }

  MoveBy (dx, dy) {
    this.point.IncPoint(dx, dy)
  }

  MoveTo (x, y) {
    this.point.SetPoint(x, y)
  }

  NextSide () {
    return this
  }

  GetCenter () {
    return this.point
  }

  GetSides () {
    return [this]
  }

  GetRandomPoint () {
    return this.point
  }

  GetRandomSide () {
    return this
  }

  Display (ctx) {
    CSManager.GetSystem('pointEmitShapeRenderer').Update(this, ctx)
  }

  Clone () {
    return new PointEmitShape(this.point.x, this.point.y)
  }
}

export class LineEmitShape { // implements EmitShape
  constructor (x1, y1, x2, y2) {
    this.line = new Line(x1, y1, x2, y2)
  }

  MoveBy (dx, dy) {
    this.line.IncLine(dx, dy)
  }

  MoveTo (x, y) {
    this.line.SetLine(x, y)
  }

  NextSide () {
    return this
  }

  GetCenter () {
    return this.line.GetCenter()
  }

  GetSides () {
    return [this]
  }

  GetRandomPoint () {
    if (this.line.x1 === this.line.x2) {
      return new Point(this.line.x1, RandomInt(this.line.y1, this.line.y2))
    }
    let x = RandomInt(this.line.x1, this.line.x2)
    let y = Math.floor(this.line.GetSlope() * x + this.line.GetB())
    return new Point(x, y)
  }

  GetRandomSide () {
    return this
  }

  Display (ctx) {
    CSManager.GetSystem('lineEmitShapeRenderer').Update(this, ctx)
  }

  Clone () {
    return new LineEmitShape(this.line.x1, this.line.y1, this.line.x2, this.line.y2)
  }
}

export class RectEmitShape { // implements EmitShape
  constructor (x1, y1, x2, y2, _edgeEmit) {
    this.rect = new Rect(x1, y1, x2, y2)
    this.edgeEmit = _edgeEmit
    this.sides = [new LineEmitShape(x1, y1, x2, y1), new LineEmitShape(x2, y1, x2, y2), new LineEmitShape(x2, y2, x1, y2), new LineEmitShape(x1, y2, x1, y1)]
    this.index = 0
  }

  MoveBy (dx, dy) {
    this.rect.IncRect(dx, dy)
    this.sides.forEach((el) => { el.MoveBy(dx, dy) })
  }

  MoveTo (x, y) {
    this.rect.SetRect(x, y)
    this.sides.forEach((el) => { el.MoveTo(x, y) })
  }

  NextSide () {
    let side = this.sides[this.index]
    this.index++
    if (this.index === this.sides.length) this.index = 0
    return side
  }

  GetCenter () {
    return this.rect.GetCenter()
  }

  GetSides () {
    return this.sides
  }

  GetRandomPoint () {
    if (this.edgeEmit) {
      let x = this.rect.GetX()
      let y = this.rect.GetY()
      let width = this.rect.GetWidth()
      let height = this.rect.GetHeight()
      let r = RandomInt(0, 2 * width + 2 * height)
      return (r < width ? new Point(x + r, y) // top
        : r < width + height ? new Point(x + width, y + r - width) // right
          : r < 2 * width + height ? new Point(x + r - (width + height), y + height) // bottom
            : new Point(x, y + r - (2 * width + height))) // left
    }
    // else
    return new Point(RandomInt(this.rect.x1, this.rect.x2), RandomInt(this.rect.y1, this.rect.y2))
  }

  GetRandomSide () {
    let r = RandomInt(0, this.sides.length)
    return this.sides[r]
  }

  Display (ctx) {
    CSManager.GetSystem('rectEmitShapeRenderer').Update(this, ctx)
  }

  Clone () {
    return new RectEmitShape(this.rect.x1, this.rect.x1, this.rect.y1, this.rect.y2, this.edgeEmit)
  }
}

export class CircleEmitShape { // implements EmitShape
  constructor (x, y, radius, _edgeEmit) {
    this.circle = new Circle(x, y, radius)
    this.edgeEmit = _edgeEmit
  }

  MoveBy (dx, dy) {
    this.circle.IncCircle(dx, dy)
  }

  MoveTo (x, y) {
    this.circle.SetCircle(x, y)
  }

  NextSide () {
    return this
  }

  GetCenter () {
    return this.circle.GetCenter()
  }

  GetSides () {
    return [this]
  }

  GetRandomPoint () {
    let angle = RandomNum(0, 2 * Math.PI)
    if (this.edgeEmit) {
      return new Point(this.GetCenter().x + this.circle.radius * Math.cos(angle), this.GetCenter().y + this.circle.radius * Math.sin(angle))
    }
    // else
    let r = RandomNum(0, this.circle.radius)
    return new Point(this.GetCenter().x + r * Math.cos(angle), this.GetCenter().y + r * Math.sin(angle))
  }

  GetRandomSide () {
    return this
  }

  Display (ctx) {
    CSManager.GetSystem('circleEmitShapeRenderer').Update(this, ctx)
  }

  Clone () {
    return new CircleEmitShape(this.circle.x, this.circle.y, this.circle.radius, this.edgeEmit)
  }
}
