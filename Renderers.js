import { CSManager } from './CSManager.js'

class ColorRenderer {
  Update (particle, ctx) {
    if (typeof particle.colorComponent === 'undefined') return
    let c = [
      Math.floor(particle.colorComponent.color[0]),
      Math.floor(particle.colorComponent.color[1]),
      Math.floor(particle.colorComponent.color[2]),
      particle.colorComponent.color[3]
    ]

    ctx.fillStyle = 'rgba(' + c[0] + ',' + c[1] + ',' + c[2] + ',' + c[3] + ')'
    ctx.strokeStyle = 'rgba(' + c[0] + ',' + c[1] + ',' + c[2] + ',' + c[3] + ')'
  }
}

class SplatRenderer {
  Update (particle, ctx) {
    if (typeof particle.colorComponent === 'undefined' || typeof particle.splatComponent === 'undefined') return
    let pos = particle.position
    let r = {}
    if (typeof particle.circleRenderer !== 'undefined') {
      r = particle.circleComponent.radius
    } else if (typeof particle.rectangleRenderer !== 'undefined') {
      let rect = particle.rectangleComponent
      r = Math.min(rect.width, rect.height)
    } else {
      return
    }
    let normRadius = particle.splatComponent.normRadius
    let c = [
      Math.floor(particle.colorComponent.color[0]),
      Math.floor(particle.colorComponent.color[1]),
      Math.floor(particle.colorComponent.color[2]),
      particle.colorComponent.color[3]
    ]
    let bc = [
      Math.floor(particle.splatComponent.backColor[0]),
      Math.floor(particle.splatComponent.backColor[1]),
      Math.floor(particle.splatComponent.backColor[2]),
      particle.splatComponent.backColor[3]
    ]

    if (normRadius === 1) normRadius = 0.999 // createRadialGradient() same circle radius fix
    let gradient = ctx.createRadialGradient(Math.floor(pos.x), Math.floor(pos.y), normRadius * r, Math.floor(pos.x), Math.floor(pos.y), r)

    gradient.addColorStop(0, 'rgba(' + c[0] + ',' + c[1] + ',' + c[2] + ',' + c[3] + ')')
    gradient.addColorStop(1, 'rgba(' + bc[0] + ',' + bc[1] + ',' + bc[2] + ',' + bc[3] + ')')

    ctx.fillStyle = gradient
    ctx.strokeStyle = 'rgba(0,0,0,0)'
  }
}

class ScaleRenderer {
  Update (particle, ctx) {
    if (typeof particle.scaleComponent === 'undefined') return
    let pos = particle.position
    let scale = particle.scaleComponent.scale
    ctx.translate(pos.x, pos.y)
    ctx.scale(scale, scale)
    ctx.translate(-pos.x, -pos.y)
  }
}

class RotationRenderer {
  Update (particle, ctx) {
    if (typeof particle.rotationComponent === 'undefined') return
    let pos = particle.position
    let rotation = particle.rotationComponent.rotation
    ctx.translate(pos.x, pos.y)
    ctx.rotate(rotation)
    ctx.translate(-pos.x, -pos.y)
  }
}

class CircleRenderer {
  Update (particle, ctx) {
    if (typeof particle.circleComponent === 'undefined') return
    let pos = particle.position
    let circle = particle.circleComponent
    ctx.beginPath()
    ctx.arc(Math.floor(pos.x), Math.floor(pos.y), circle.radius, 0, 2 * Math.PI)
    ctx.stroke()
    ctx.fill()
  }
}

class RectangleRenderer {
  Update (particle, ctx) {
    if (typeof particle.rectangleComponent === 'undefined') return
    let pos = particle.position
    let rect = particle.rectangleComponent
    ctx.beginPath()
    ctx.rect(Math.floor(pos.x - rect.width / 2), Math.floor(pos.y - rect.height / 2), rect.width, rect.height)
    ctx.fill()
    ctx.stroke()
  }
}

class PointEmitShapeRenderer {
  Update (emitShape, ctx) {
    ctx.beginPath()
    ctx.strokeStyle = ctx.fillStyle = 'black'
    ctx.arc(emitShape.point.x, emitShape.point.y, 2, 0, 2 * Math.PI)
    ctx.fill()
    ctx.stroke()
  }
}

class LineEmitShapeRenderer {
  Update (emitShape, ctx) {
    ctx.beginPath()
    ctx.strokeStyle = ctx.fillStyle = 'black'
    ctx.moveTo(emitShape.line.x1, emitShape.line.y1)
    ctx.lineTo(emitShape.line.x2, emitShape.line.y2)
    ctx.stroke()
    ctx.beginPath()
    ctx.arc(emitShape.GetCenter().x, emitShape.GetCenter().y, 2, 0, 2 * Math.PI)
    ctx.fill()
    ctx.stroke()
  }
}

class RectEmitShapeRenderer {
  Update (emitShape, ctx) {
    ctx.beginPath()
    ctx.strokeStyle = ctx.fillStyle = 'black'
    ctx.rect(emitShape.rect.GetX(), emitShape.rect.GetY(), emitShape.rect.GetWidth(), emitShape.rect.GetHeight())
    ctx.stroke()
    ctx.beginPath()
    ctx.arc(emitShape.GetCenter().x, emitShape.GetCenter().y, 2, 0, 2 * Math.PI)
    ctx.fill()
    ctx.stroke()
  }
}

class CircleEmitShapeRenderer {
  Update (emitShape, ctx) {
    ctx.beginPath()
    ctx.strokeStyle = ctx.fillStyle = 'black'
    ctx.arc(emitShape.circle.x, emitShape.circle.y, emitShape.circle.radius, 0, 2 * Math.PI)
    ctx.stroke()
    ctx.beginPath()
    ctx.arc(emitShape.GetCenter().x, emitShape.GetCenter().y, 2, 0, 2 * Math.PI)
    ctx.fill()
    ctx.stroke()
  }
}

export function RegisterRenderers () {
  CSManager.RegisterSystem('colorRenderer', new ColorRenderer())
  CSManager.RegisterSystem('splatRenderer', new SplatRenderer())
  CSManager.RegisterSystem('scaleRenderer', new ScaleRenderer())
  CSManager.RegisterSystem('rotationRenderer', new RotationRenderer())
  CSManager.RegisterSystem('circleRenderer', new CircleRenderer())
  CSManager.RegisterSystem('rectangleRenderer', new RectangleRenderer())
  CSManager.RegisterSystem('pointEmitShapeRenderer', new PointEmitShapeRenderer())
  CSManager.RegisterSystem('lineEmitShapeRenderer', new LineEmitShapeRenderer())
  CSManager.RegisterSystem('rectEmitShapeRenderer', new RectEmitShapeRenderer())
  CSManager.RegisterSystem('circleEmitShapeRenderer', new CircleEmitShapeRenderer())
}
