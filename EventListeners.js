import { Point } from './Shapes.js'

export function Clickable (el, system) {
  if (el.clickFlag) return
  el.clickFlag = true
  system.spawnerComponent.spawnDelay = -1
  el.addEventListener('click', clickListener)
  function clickListener (event) {
    event.preventDefault()
    let currTime = Date.now()
    let spawnDelay = system.spawnerComponent.spawnDelay
    system.spawnerComponent.spawnDelay = 0
    system.Spawn(currTime)
    system.spawnerComponent.spawnDelay = spawnDelay
  }
}

export function Draggable (el, system) {
  if (el.dragFlag) return
  el.dragFlag = true
  let oldPos = new Point(0, 0)
  let dragDist = new Point(0, 0)
  system.spawnerComponent.spawnDelay = -1
  let spawnDelay = {}
  el.addEventListener('mousedown', dragMouseDown)

  function dragMouseDown (event) {
    event.preventDefault()
    oldPos.x = event.clientX
    oldPos.y = event.clientY
    document.addEventListener('mouseup', closeDragElement)
    document.addEventListener('mousemove', elementDrag)

    spawnDelay = system.spawnerComponent.spawnDelay
    system.spawnerComponent.spawnDelay = 1 * 1000
  }

  function elementDrag (event) {
    event.preventDefault()
    // calculate drag distance
    dragDist.x = event.clientX - oldPos.x
    dragDist.y = event.clientY - oldPos.y
    oldPos.x = event.clientX
    oldPos.y = event.clientY
    // set the element's new position:
    el.style.left = (el.offsetLeft + dragDist.x) + 'px'
    el.style.top = (el.offsetTop + dragDist.y) + 'px'
    system.emitArea.MoveBy(dragDist.x, dragDist.y)

    let currTime = Date.now()
    system.Spawn(currTime)
  }

  function closeDragElement (event) {
    event.preventDefault()
    // stop moving when mouse button is released:
    document.removeEventListener('mouseup', closeDragElement)
    document.removeEventListener('mousemove', elementDrag)

    system.spawnerComponent.spawnDelay = spawnDelay
  }
}
