export class Point {
  constructor (_x, _y) {
    this.SetPoint(_x, _y)
  }

  SetPoint (_x, _y) {
    this.x = _x
    this.y = _y
  }

  IncPoint (dx, dy) {
    this.x += dx
    this.y += dy
  }

  Clone () {
    return new Point(this.x, this.y)
  }
}

export class Line {
  constructor (_x1, _y1, _x2, _y2) {
    this.x1 = _x1
    this.y1 = _y1
    this.x2 = _x2
    this.y2 = _y2
  }

  GetSlope () {
    return (this.x1 !== this.x2) ? (this.y1 - this.y2) / (this.x1 - this.x2) : 0
  }

  GetB () {
    return (this.y1 - (this.GetSlope() * this.x1))
  }

  SetLine (_x, _y) {
    let dx = _x - this.x1
    let dy = _y - this.y1
    this.IncLine(dx, dy)
  }

  IncLine (dx, dy) {
    this.x1 += dx
    this.y1 += dy
    this.x2 += dx
    this.y2 += dy
  }

  GetCenter () {
    return new Point((this.x1 + this.x2) / 2, (this.y1 + this.y2) / 2)
  }

  Clone () {
    return new Line(this.x1, this.y1, this.x2, this.y2)
  }
}

export class Rect {
  constructor (_x1, _y1, _x2, _y2) {
    this.x1 = _x1
    this.y1 = _y1
    this.x2 = _x2
    this.y2 = _y2
  }

  Contains (x, y) {
    if (x < this.x1 ||
      y < this.y1 ||
      x > this.x2 ||
      y > this.y2) return false
    return true
  }

  SetRect (_x, _y) {
    let dx = _x - this.x1
    let dy = _y - this.y1
    this.IncRect(dx, dy)
  }

  IncRect (dx, dy) {
    this.x1 += dx
    this.y1 += dy
    this.x2 += dx
    this.y2 += dy
  }

  GetX () {
    return this.x1
  }
  GetY () {
    return this.y1
  }
  GetWidth () {
    return Math.abs(this.x2 - this.x1)
  }
  GetHeight () {
    return Math.abs(this.y2 - this.y1)
  }
  GetCenter () {
    return new Point(this.GetX() + this.GetWidth() / 2, this.GetY() + this.GetHeight() / 2)
  }

  Clone () {
    return new Rect(this.x1, this.y1, this.x2, this.y2)
  }
}

export class Circle {
  constructor (_x, _y, _radius) {
    this.x = _x
    this.y = _y
    this.radius = _radius
  }

  SetCircle (_x, _y) {
    let dx = _x - this.x
    let dy = _y - this.y
    this.IncCircle(dx, dy)
  }

  IncCircle (dx, dy) {
    this.x += dx
    this.y += dy
  }

  GetCenter () {
    return new Point(this.x, this.y)
  }

  Clone () {
    return new Circle(this.x, this.y, this.radius)
  }
}
