import { DeepClone, AttachInitRange } from './Utilities.js'

class _CSManager {
  constructor () {
    this.components = {}
    this.systems = {}
  }
  RegisterComponent (name, component) {
    this.components[name] = component
  }
  RegisterSystem (name, system) {
    this.systems[name] = system
  }
  GetComponent (componentName) {
    let componentRef = this.components[componentName]
    console.assert(typeof componentRef !== 'undefined', componentName + ' not found')
    let component = DeepClone(componentRef)
    return component
  }
  GetSystem (systemName) {
    let system = this.systems[systemName]
    console.assert(typeof system !== 'undefined', systemName + ' not found')
    return system
  }
  GetInitializer (componentName) {
    let initializer = this.GetComponent(componentName)
    console.assert(typeof initializer !== 'undefined', componentName + ' not found')
    AttachInitRange(initializer)
    return initializer
  }
  /* GetComponent (componentName, initializer) {
    let componentRef = this.components[componentName]
    console.assert(typeof componentRef !== 'undefined', componentName + ' not found')
    let component = DeepClone(componentRef)
    if (typeof initializer !== 'undefined') {
      DeepInit(component, initializer)
    }
    return component
  } */
  /*  GetComponents (initializers) {
    let comps = {}
    Object.entries(this.components).forEach(([key, value]) => {
      comps[key] = this.GetComponent(key, initializers[key.replace('Component', 'Initializer')])
    })
    return comps
  }
  GetComponentNames () {
    return Object.keys(this.components)
  } */
}
export const CSManager = new _CSManager()
