import { RandomInt } from './Utilities.js'

export class EmitArea {
  constructor (_emitShapesArray) {
    this.emitShapesArray = _emitShapesArray
    this.index = 0
  }

  InsertStart (_shape) {
    this.emitShapesArray.unshift(_shape)
  }
  InsertEnd (_shape) {
    this.emitShapesArray.push(_shape)
  }
  RemoveStart () {
    return this.emitShapesArray.shift()
  }
  RemoveEnd () {
    return this.emitShapesArray.pop()
  }

  MoveBy (dx, dy) {
    this.emitShapesArray.forEach(function (emitShape, i) {
      emitShape.MoveBy(dx, dy)
    }, this)
  }

  MoveTo (x, y) {
    this.emitShapesArray.forEach(function (emitShape, i) {
      emitShape.MoveTo(x, y)
    }, this)
  }

  NextShape () {
    let shape = this.emitShapesArray[this.index]
    this.index++
    if (this.index === this.emitShapesArray.length) this.index = 0
    return shape
  }

  NextSide () {
    return this.emitShapesArray[this.index]
  }

  GetCenter () {
    return this.emitShapesArray[this.index].GetCenter()
  }

  GetRandomPoint () {
    let r = RandomInt(0, this.emitShapesArray.length)
    return this.emitShapesArray[r].GetRandomPoint()
  }

  GetRandomSide () {
    return this.emitShapesArray[this.index]
  }

  GetShapes () {
    return this.emitShapesArray
  }

  GetSides () {
    return this.emitShapesArray[this.index].GetSides()
  }

  DisplayShapes (ctx) {
    this.emitShapesArray.forEach(function (emitShape, i) {
      emitShape.Display(ctx)
    }, this)
  }

  Clone () {
    let clonedShapesArray = []
    this.emitShapesArray.forEach(function (shape, i) {
      clonedShapesArray.push(shape.Clone())
    }, this)
    return new EmitArea(clonedShapesArray)
  }
}
