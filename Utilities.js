export class InitRange {
  constructor (_min, _max) {
    this.Set(_min, _max)
  }
  Set (_min, _max) {
    _min = _min || 0
    _max = _max || _min
    this.min = _min
    this.max = _max
  }
}

export class InitRangeArray {
  constructor (_min, _max) {
    this.Set(_min, _max)
  }
  Set (_min, _max) {
    _min = _min || [0, 0, 0, 1.0]
    _max = _max || _min.slice(0) // interesting bug (fixed)
    this.min = _min
    this.max = _max
  }
}

export function RandomNum (a, b) {
  let min = Math.min(a, b)
  let max = Math.max(a, b)
  return (Math.random() * (max - min) + min)
}

export function RandomInt (a, b) {
  return Math.floor(RandomNum(a, b))
}

export function GetDirection (center, position, flip) {
  let dir = Math.atan2(position.y - center.y, position.x - center.x)
  return (flip === false ? dir : dir + Math.PI)
}

export function RadiansToDegrees (radians) {
  return (radians * 180 / Math.PI) % 360
}

export function DeepClone (comp, emptyArray) {
  let obj = emptyArray || {}
  for (let i in comp) {
    let prop = comp[i]
    if (prop === obj) continue
    if (typeof prop === 'object') {
      if (Array.isArray(prop)) {
        obj[i] = DeepClone(prop, []) // array
      } else if (prop instanceof InitRange) {
        let o = new InitRange()
        Object.assign(o, comp[i])
        obj[i] = o
      } else if (prop instanceof InitRangeArray) {
        let o = new InitRangeArray()
        Object.assign(o, comp[i])
        obj[i] = o
      } else {
        obj[i] = Object.create(prop) // object
      }
    } else {
      obj[i] = prop // other value
    }
  }
  return obj
}

/* export function DeepClone (comp) {
  let clone = Object.assign({}, comp)
  Object.entries(comp).forEach(([key, value]) => {
    if (clone[key] != null && typeof value === 'object') {
      clone[key] = DeepClone(value)
    }
  })
  return clone
} */

export function DeepInit (comp, init) {
  Object.keys(comp).forEach((key) => {
    if (init[key] instanceof InitRange) {
      console.assert(typeof comp[key] !== 'object', 'DeepInit: assert typeof comp[key] !== object')
      comp[key] = RandomNum(init[key].min, init[key].max)
    } else if (init[key] instanceof InitRangeArray) {
      console.assert(Array.isArray(comp[key]), 'DeepInit: assert Array.isArray(comp[key])')
      comp[key].forEach((el, i, arr) => { arr[i] = RandomNum(init[key].min[i], init[key].max[i]) })
    } else if (Array.isArray(init[key])) {
      console.assert(Array.isArray(comp[key], 'DeepInit: assert Array.isArray(comp[key]'))
      comp[key].forEach((el, i, arr) => { DeepInit(arr[i], init[i]) })
    } else {
      console.assert(typeof init[key] === 'object' && typeof comp[key] === 'object', 'DeepInit: assert typeof init[key] === object && typeof comp[key] === object')
      DeepInit(comp[key], init[key])
    }
  })
}

export function AttachInitRange (comp) {
  Object.keys(comp).forEach((key) => {
    if (typeof comp[key] !== 'object' || comp[key] === null) {
      comp[key] = new InitRange()
    } else if (Array.isArray(comp[key]) && comp[key].length === 4) {
      comp[key] = new InitRangeArray()
    } else if (Array.isArray(comp[key])) {
      comp[key].forEach((el) => { AttachInitRange(el) })
    } else {
      AttachInitRange(comp[key])
    }
  })
}
