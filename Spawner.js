import { CSManager } from './CSManager.js'
import { Particle } from './Particle.js'
import { GetDirection, RandomInt, DeepInit } from './Utilities.js'

function InitializeComponents (particle, initializers) {
  Object.keys(initializers).forEach((key, i) => {
    DeepInit(particle[key], initializers[key])
  })
  particle.ringMotionComponent.center.x = particle.position.x
  particle.ringMotionComponent.center.y = particle.position.y
  particle.waveMotionComponent.center.x = particle.position.x
  particle.waveMotionComponent.center.y = particle.position.y
}

class Spawner {
  Update (particleSystem, currTime) {
    if (typeof particleSystem.spawnerComponent === 'undefined') return
    let spawner = particleSystem.spawnerComponent

    // spawn mode: disabled spawner, instant spawn, delay spawn
    let dt = {}
    if (spawner.spawnDelay < 0) {
      // disabled spawner
      return
    } else if (spawner.spawnDelay > 0) {
      // delay spawn
      let elapsedTime = currTime - spawner.lastTime
      // dt = Math.floor(elapsedTime / spawner.spawnDelay)
      dt = 1 // force 1*spawnRate particles per Spawner call
      if (elapsedTime < spawner.spawnDelay) return // to early to spawn
      spawner.lastTime = currTime
    } else {
      // instant spawn
      dt = 1
    }

    let hasTank = (spawner.maxTank > 0)
    let emitTank = (spawner.spawnRate === 0)
    console.assert((hasTank || !emitTank), 'Spawner, hasTank === ' + hasTank + ',emitTank === ' + emitTank)

    if (hasTank && spawner.tank === 0) return // no more particles in tank

    // emit tank or spawn in bundles
    let spawnCount = (emitTank) ? spawner.maxTank : spawner.spawnRate * dt

    if (hasTank && !emitTank && spawnCount > spawner.tank) {
      // empty last particles in tank
      spawnCount = spawner.tank
    }

    // particles to be spawned exceed maxAlive limit
    if (particleSystem.aliveParticles.length + spawnCount > spawner.maxAlive) return

    // pick emit area shape
    let shape = {}
    switch (particleSystem.spawnShape) {
      case -2:
        // random point from all emit area shapes
        shape = particleSystem.emitArea
        break
      case -1:
        // next shape on each particle spawn
        shape = particleSystem.emitArea.NextShape()
        break
      default:
        shape = particleSystem.emitArea.GetShapes()[particleSystem.spawnShape]
        break
    }
    // pick emit shape side
    let side = {}
    switch (particleSystem.spawnSide) {
      case -2:
        // random point from all emit area shapes
        side = shape
        break
      case -1:
        // next side on each particle spawn
        side = shape.NextSide()
        break
      default:
        let sides = shape.GetSides()
        side = sides[Math.min(sides.length - 1, particleSystem.spawnSide)]
        break
    }

    // Spawn each particle
    for (let i = 0; i < spawnCount; i++) {
      let spawnPoint = side.GetRandomPoint()
      let lifeTime = RandomInt(particleSystem.lifeTime.min, particleSystem.lifeTime.max)
      // Recycle
      let particle = particleSystem.deadParticles.shift()
      if (typeof particle === 'undefined') {
        particle = new Particle(spawnPoint.x, spawnPoint.y, lifeTime, particleSystem.systems)
      } else {
        particle.Init(spawnPoint.x, spawnPoint.y, lifeTime, particleSystem.systems)
      }
      // Initalize particle components
      InitializeComponents(particle, particleSystem.componentInitializers)
      if (particleSystem.relativeEmitDirection === 'centerout') {
        particle.velocityComponent.direction = GetDirection(shape.GetCenter(), spawnPoint, false)
      } else if (particleSystem.relativeEmitDirection === 'centerin') {
        particle.velocityComponent.direction = GetDirection(shape.GetCenter(), spawnPoint, true)
      }
      // Insert particle in alive particles list
      particle.Start(currTime)
      particleSystem.aliveParticles.push(particle)
    }
    if (hasTank) spawner.tank -= spawnCount
    console.assert(spawner.tank >= 0, 'spawner.tank===' + spawner.tank)
  }
}

export function RegisterSpawner () {
  CSManager.RegisterSystem('spawner', new Spawner())
}
