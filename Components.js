import { CSManager } from './CSManager.js'

class SpawnerComponent {
  constructor () {
    this.maxTank = 0
    this.maxAlive = 0
    this.spawnRate = 0
    this.spawnDelay = 0
    this.tank = 0
    this.lastTime = 0
  }
}

class ColorComponent {
  constructor () {
    this.color = [0, 0, 0, 1.0]
    this.start = [0, 0, 0, 1.0]
    this.end = [0, 0, 0, 1.0]
    this.source = [0, 0, 0, 1.0]
    this.modulation = [0, 0, 0, 1.0]
  }
}

class SplatComponent {
  constructor () {
    this.backColor = [0, 0, 0, 1.0]
    this.normRadius = 0.5
  }
}

class ScaleComponent {
  constructor () {
    this.scale = 0
    this.speed = 0
    this.start = 0
    this.end = 0
    this.min = 0
    this.max = 0
  }
}

class RotationComponent {
  constructor () {
    this.rotation = 0
    this.speed = 0
    this.start = 0
    this.end = 0
    this.min = 0
    this.max = 0
  }
}

class VelocityComponent {
  constructor () {
    this.speed = 0
    this.acceleration = 0
    this.direction = 0
    this.start = 0
    this.end = 0
    this.min = 0
    this.max = 0
  }
}

class RingMotionComponent {
  constructor () {
    this.center = { x: 0, y: 0 }
    this.angle = 0
    this.angularSpeed = 0
    this.angularAcceleration = 0
    this.ringRadius = 0
    this.ringSpeed = 0
  }
}

class WaveMotionComponent {
  constructor () {
    this.pos = 0
    this.center = { x: 0, y: 0 }
    this.amplitude = 0
    this.amplitudeSpeed = 0
    this.angle = 0
    this.frequency = 0
    this.frequencySpeed = 0
    this.rotation = 0
    this.rotationSpeed = 0
  }
}

class CircleComponent {
  constructor () {
    this.radius = 10
  }
}

class RectangleComponent {
  constructor () {
    this.width = 10
    this.height = 10
  }
}

export function RegisterComponents () {
  CSManager.RegisterComponent('spawnerComponent', new SpawnerComponent())
  CSManager.RegisterComponent('colorComponent', new ColorComponent())
  CSManager.RegisterComponent('splatComponent', new SplatComponent())
  CSManager.RegisterComponent('scaleComponent', new ScaleComponent())
  CSManager.RegisterComponent('rotationComponent', new RotationComponent())
  CSManager.RegisterComponent('velocityComponent', new VelocityComponent())
  CSManager.RegisterComponent('ringMotionComponent', new RingMotionComponent())
  CSManager.RegisterComponent('waveMotionComponent', new WaveMotionComponent())
  CSManager.RegisterComponent('circleComponent', new CircleComponent())
  CSManager.RegisterComponent('rectangleComponent', new RectangleComponent())
}
