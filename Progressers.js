import { CSManager } from './CSManager.js'

class ScaleProgresser {
  Update (particle, currTime) {
    if (typeof particle.scaleComponent === 'undefined') return
    let scaleComp = particle.scaleComponent
    let limitsEnabled = (scaleComp.min !== 0 || scaleComp.max !== 0)
    let dt = currTime - particle.lastTime

    let scaleShift = scaleComp.speed * dt
    scaleComp.scale = (scaleComp.scale + scaleShift)
    if (limitsEnabled) {
      if (scaleComp.scale > scaleComp.max) scaleComp.scale = scaleComp.max
      else if (scaleComp.scale < scaleComp.min) scaleComp.scale = scaleComp.min
    }
  }
}

class RotationProgresser {
  Update (particle, currTime) {
    if (typeof particle.rotationComponent === 'undefined') return
    let rotationComp = particle.rotationComponent
    let limitsEnabled = (rotationComp.min !== 0 || rotationComp.max !== 0)
    let dt = currTime - particle.lastTime

    let rotationShift = rotationComp.speed * dt
    rotationComp.rotation = (rotationComp.rotation + rotationShift)

    if (limitsEnabled) {
      if (rotationComp.rotation > rotationComp.max) rotationComp.rotation = rotationComp.max
      else if (rotationComp.rotation < rotationComp.min) rotationComp.rotation = rotationComp.min
    }
  }
}

class ColorByLifeTimeProgresser {
  Update (particle, currTime) {
    if (typeof particle.colorComponent === 'undefined') return
    let colorComp = particle.colorComponent
    let factor = particle.GetNormalizedAge()

    colorComp.color.forEach((el, i, arr) => {
      arr[i] = colorComp.start[i] + (colorComp.end[i] - colorComp.start[i]) * factor
    })
  }
}

class ColorBySpeedProgresser {
  Update (particle, currTime) {
    if (typeof particle.colorComponent === 'undefined' || typeof particle.velocityComponent === 'undefined') return
    let colorComp = particle.colorComponent
    let factor = particle.GetNormalizedSpeed()

    colorComp.color.forEach((el, i, arr) => {
      arr[i] = colorComp.start[i] + (colorComp.end[i] - colorComp.start[i]) * factor
    })
  }
}

class ColorModulationByLifeTimeProgresser {
  Update (particle, currTime) {
    if (typeof particle.colorComponent === 'undefined') return
    let source = particle.colorComponent.source
    let mod = particle.colorComponent.modulation
    let factor = particle.GetNormalizedAge()

    particle.colorComponent.color.forEach((el, i, arr) => {
      // arr[i] = source[i] * (1 - factor) + source[i] * mod[i] * factor
      arr[i] = source[i] + source[i] * mod[i] * factor
    })
  }
}

class ScaleByLifeTimeProgresser {
  Update (particle, currTime) {
    if (typeof particle.scaleComponent === 'undefined') return
    let scaleComp = particle.scaleComponent
    let factor = particle.GetNormalizedAge()

    scaleComp.scale = scaleComp.start + (scaleComp.end - scaleComp.start) * factor
  }
}

class ScaleBySpeedProgresser {
  Update (particle, currTime) {
    if (typeof particle.scaleComponent === 'undefined' || typeof particle.velocityComponent === 'undefined') return
    let scaleComp = particle.scaleComponent
    let factor = particle.GetNormalizedSpeed()

    scaleComp.scale = scaleComp.start + (scaleComp.end - scaleComp.start) * factor
  }
}

class RotationByLifeTimeProgresser {
  Update (particle, currTime) {
    if (typeof particle.rotationComponent === 'undefined') return
    let rotationComp = particle.rotationComponent
    let factor = particle.GetNormalizedAge()

    rotationComp.rotation = rotationComp.start + (rotationComp.end - rotationComp.start) * factor
  }
}

class RotationBySpeedProgresser {
  Update (particle, currTime) {
    if (typeof particle.rotationComponent === 'undefined' || typeof particle.velocityComponent === 'undefined') return
    let rotationComp = particle.rotationComponent
    let factor = particle.GetNormalizedSpeed()

    rotationComp.rotation = rotationComp.start + (rotationComp.end - rotationComp.start) * factor
  }
}

class LinearMotionProgresser {
  Update (particle, currTime) {
    if (typeof particle.velocityComponent === 'undefined') return
    let velocity = particle.velocityComponent
    let limitsEnabled = (velocity.min !== 0 || velocity.max !== 0)
    let dt = currTime - particle.lastTime

    velocity.speed = velocity.speed + velocity.acceleration * dt
    if (limitsEnabled) {
      if (velocity.speed > velocity.max) velocity.speed = velocity.max
      else if (velocity.speed < velocity.min) velocity.speed = velocity.min
    }

    let linearShift = { x: 0, y: 0 }
    linearShift.x = velocity.speed * Math.cos(velocity.direction) * dt // is v_fx * dt
    linearShift.y = velocity.speed * Math.sin(velocity.direction) * dt // is v_fy * dt

    particle.position.x = particle.position.x + linearShift.x // x_fx = x_ix + v_fx * dt
    particle.position.y = particle.position.y + linearShift.y // x_fy = x_iy + v_fy * dt
    if (typeof particle.ringMotionComponent !== 'undefined') {
      particle.ringMotionComponent.center.x = particle.position.x
      particle.ringMotionComponent.center.y = particle.position.y
    }
    if (typeof particle.waveMotionComponent !== 'undefined') {
      particle.waveMotionComponent.center.x = particle.position.x
      particle.waveMotionComponent.center.y = particle.position.y
    }
  }
}

class RingMotionProgresser {
  Update (particle, currTime) {
    if (typeof particle.ringMotionComponent === 'undefined') return
    let ringMotion = particle.ringMotionComponent
    let dt = currTime - particle.lastTime

    ringMotion.ringRadius = ringMotion.ringRadius + ringMotion.ringSpeed * dt // r_f = r_i + er * dt

    ringMotion.angularSpeed = ringMotion.angularSpeed + ringMotion.angularAcceleration * dt // w_f = w_i + a * dt
    let angleShift = ringMotion.angularSpeed * dt // is  w_f * dt
    ringMotion.angle = (ringMotion.angle + angleShift) % (2 * Math.PI) // w_f = w_i + a * dt

    particle.position.x = ringMotion.center.x + ringMotion.ringRadius * Math.cos(ringMotion.angle) // x_fx = x_ix + r_f * w_fx * dt
    particle.position.y = ringMotion.center.y + ringMotion.ringRadius * Math.sin(ringMotion.angle) // x_fy = x_iy + r_f * w_fy * dt

    if (typeof particle.waveMotionComponent !== 'undefined') {
      particle.waveMotionComponent.center.x = particle.position.x
      particle.waveMotionComponent.center.y = particle.position.y
    }
  }
}

class WaveMotionProgresser {
  Update (particle, currTime) {
    if (typeof particle.waveMotionComponent === 'undefined') return
    let waveMotion = particle.waveMotionComponent
    let dt = currTime - particle.lastTime

    waveMotion.amplitude = waveMotion.amplitude + waveMotion.amplitudeSpeed * dt
    waveMotion.frequency = waveMotion.frequency + waveMotion.frequencySpeed * dt
    waveMotion.rotation = waveMotion.rotation + waveMotion.rotationSpeed * dt
    let angleShift = waveMotion.frequency * dt
    waveMotion.angle = (waveMotion.angle + angleShift) % (2 * Math.PI)
    waveMotion.pos = /* waveMotion.pos + */ waveMotion.amplitude * Math.cos(waveMotion.angle)

    particle.position.x = waveMotion.center.x + waveMotion.pos * Math.cos(waveMotion.rotation)
    particle.position.y = waveMotion.center.y + waveMotion.pos * Math.sin(waveMotion.rotation)
  }
}

export function RegisterProgressers () {
  CSManager.RegisterSystem('scaleProgresser', new ScaleProgresser())
  CSManager.RegisterSystem('rotationProgresser', new RotationProgresser())
  CSManager.RegisterSystem('colorByLifeTimeProgresser', new ColorByLifeTimeProgresser())
  CSManager.RegisterSystem('colorBySpeedProgresser', new ColorBySpeedProgresser())
  CSManager.RegisterSystem('colorModulationByLifeTimeProgresser', new ColorModulationByLifeTimeProgresser())
  CSManager.RegisterSystem('scaleByLifeTimeProgresser', new ScaleByLifeTimeProgresser())
  CSManager.RegisterSystem('scaleBySpeedProgresser', new ScaleBySpeedProgresser())
  CSManager.RegisterSystem('rotationByLifeTimeProgresser', new RotationByLifeTimeProgresser())
  CSManager.RegisterSystem('rotationBySpeedProgresser', new RotationBySpeedProgresser())
  CSManager.RegisterSystem('linearMotionProgresser', new LinearMotionProgresser())
  CSManager.RegisterSystem('ringMotionProgresser', new RingMotionProgresser())
  CSManager.RegisterSystem('waveMotionProgresser', new WaveMotionProgresser())
}
