import { PointEmitShape, LineEmitShape, RectEmitShape, CircleEmitShape } from './EmitShapes.js'
import { Draggable, Clickable } from './EventListeners.js'
/* global dat */

/*
function ReadJSON (_GUI, url) {
  var xReq = new XMLHttpRequest()
  xReq.open('GET', url, false)
  xReq.setRequestHeader('Content-type', 'application/json')
  xReq.onreadystatechange = () => {
    if (xReq.readyState === 4 && xReq.status === 200) {
      _GUI.jsonObj = JSON.parse(xReq.responseText)
    } else {
      _GUI.jsonObj = null
    }
  }
  xReq.send()
}

function BuildComponentGUI (topLevelGui, folder, comp, key) {
  let curFolder = folder.addFolder(key)
  if (comp instanceof InitRange) {
    // topLevelGui.remember(comp)
    curFolder.add(comp, 'min')
    curFolder.add(comp, 'max')
  } else if (comp instanceof InitRangeArray) {
    // topLevelGui.remember(comp)
    curFolder.addColor(comp, 'min')
    curFolder.addColor(comp, 'max')
  } else if (Array.isArray(comp)) {
    comp.forEach((el, i) => {
      BuildComponentGUI(topLevelGui, curFolder, el, i)
    })
  } else {
    console.assert(typeof comp === 'object', 'BuildComponentGUI: assert typeof comp === object')
    Object.keys(comp).forEach((key) => {
      BuildComponentGUI(topLevelGui, curFolder, comp[key], key)
    })
  }
}
*/

function AddEmitShapeGUI (topLevelGui, emitAreaFolder, index, shape) {
  // topLevelGui.remember(shape)
  let name = shape.constructor.name
  let f = emitAreaFolder.addFolder('[' + index + '] ' + name)
  switch (name) {
    case 'PointEmitShape':
      // topLevelGui.remember(shape.point)
      f.add(shape.point, 'x').min(0).step(1)
      f.add(shape.point, 'y').min(0).step(1)
      break
    case 'LineEmitShape':
      // topLevelGui.remember(shape.line)
      f.add(shape.line, 'x1').min(0).step(1)
      f.add(shape.line, 'y1').min(0).step(1)
      f.add(shape.line, 'x2').min(0).step(1)
      f.add(shape.line, 'y2').min(0).step(1)
      break
    case 'RectEmitShape':
      // topLevelGui.remember(shape.rect)
      let setSides = () => {
        let x1, x2, y1, y2
        [x1, y1, x2, y2] = [shape.rect.x1, shape.rect.y1, shape.rect.x2, shape.rect.y2]
        shape.sides = [new LineEmitShape(x1, y1, x2, y1), new LineEmitShape(x2, y1, x2, y2), new LineEmitShape(x2, y2, x1, y2), new LineEmitShape(x1, y2, x1, y1)]
      }
      f.add(shape.rect, 'x1').min(0).step(1).onFinishChange(setSides)
      f.add(shape.rect, 'y1').min(0).step(1).onFinishChange(setSides)
      f.add(shape.rect, 'x2').min(0).step(1).onFinishChange(setSides)
      f.add(shape.rect, 'y2').min(0).step(1).onFinishChange(setSides)
      f.add(shape, 'edgeEmit')
      break
    case 'CircleEmitShape':
      // topLevelGui.remember(shape.circle)
      f.add(shape.circle, 'x').min(0).step(1)
      f.add(shape.circle, 'y').min(0).step(1)
      f.add(shape.circle, 'radius').min(0).step(1)
      f.add(shape, 'edgeEmit')
      break
    default:
      console.assert(false, 'BuildAll: Emit Area, unexpected switch case')
      break
  }
}

function BuildParticleSystemGUI (_GUI, particleSystem) {
  // ReadJSON(_GUI, particleSystem.id + '.json')
  let gui = new dat.GUI({
    name: particleSystem.id,
    closeOnTop: true,
    closed: true
    // load: _GUI.jsonObj || JSON,
    // preset: 'Default'
  })
  _GUI.guis[gui.name] = gui

  // Simple Properties
  {
    // gui.remember(particleSystem)
    gui.add(particleSystem, 'id')
    let shapes = particleSystem.emitArea.GetShapes()
    let shapeC = gui.add(particleSystem, 'spawnShape', -2, shapes.length - 1, 1).listen()
    let sideC = gui.add(particleSystem, 'spawnSide', -2, shapes[0].GetSides().length - 1, 1).listen()
    gui.add(particleSystem, 'displayEmitArea')
    gui.add(particleSystem, 'relativeEmitDirection', ['none', 'centerin', 'centerout'])
    shapeC.onChange((val) => {
      if (val >= 0) {
        let newShapes = particleSystem.emitArea.GetShapes()
        let _max = newShapes[val].GetSides().length - 1
        sideC.max(_max)
        if (sideC.getValue() > _max) sideC.setValue(_max)
      }
    })
  }
  // LifeTime
  {
    // gui.remember(particleSystem.lifeTime)
    let f = gui.addFolder('lifeTime')
    f.add(particleSystem.lifeTime, 'min').min(0).step(1)
    f.add(particleSystem.lifeTime, 'max').min(0).step(1)
  }
  // Emit Area
  {
    // gui.remember(particleSystem.emitArea)
    let areaF = gui.addFolder('emitArea')
    particleSystem.emitArea.GetShapes().forEach((shape, i) => {
      AddEmitShapeGUI(gui, areaF, i, shape)
    })
  }

  // Spawner Component
  {
    // gui.remember(particleSystem.spawnerComponent)
    let spawnerFolder = gui.addFolder('spawnerComponent')
    spawnerFolder.add(particleSystem.spawnerComponent, 'maxTank').min(0).step(1).listen().onFinishChange((val) => { particleSystem.spawnerComponent.tank = val })
    spawnerFolder.add(particleSystem.spawnerComponent, 'maxAlive').min(0).step(1).listen()
    spawnerFolder.add(particleSystem.spawnerComponent, 'spawnRate').min(0).step(1).listen()
    spawnerFolder.add(particleSystem.spawnerComponent, 'spawnDelay').min(-1).step(100).listen()
  }

  // gui.remember(particleSystem.systems)
  // gui.remember(particleSystem.componentInitializers)
  // Color
  {
    let sys = particleSystem.systems
    let color = particleSystem.componentInitializers.colorComponent
    let splat = particleSystem.componentInitializers.splatComponent
    // gui.remember(color.color)
    // gui.remember(color.start)
    // gui.remember(color.end)
    // gui.remember(color.source)
    // gui.remember(color.modulation)
    // gui.remember(splat.backColor)
    // gui.remember(splat.normRadius)
    let compF = gui.addFolder('Color')
    compF.add(sys, 'colorRenderer')
    {
      let f = compF.addFolder('color')
      f.addColor(color.color, 'min').name('colorMin').listen()
      f.add({ aMin: color.color.min[3] }, 'aMin', 0, 1, 0.05).name('alphaMin').onChange((val) => { color.color.min[3] = val })
      f.addColor(color.color, 'max').name('colorMax').listen()
      f.add({ aMax: color.color.max[3] }, 'aMax', 0, 1, 0.05).name('alphaMax').onChange((val) => { color.color.max[3] = val })
    }
    compF.add(sys, 'splatRenderer')
    {
      let f = compF.addFolder('splat')
      f.addColor(splat.backColor, 'min').name('splatBColorMin').listen()
      f.add({ aMin: splat.backColor.min[3] }, 'aMin', 0, 1, 0.05).name('alphaMin').onChange((val) => { splat.backColor.min[3] = val })
      f.addColor(splat.backColor, 'max').name('splatBColorMax').listen()
      f.add({ aMax: splat.backColor.max[3] }, 'aMax', 0, 1, 0.05).name('alphaMax').onChange((val) => { splat.backColor.max[3] = val })
      f.add(splat.normRadius, 'min', 0, 1, 0.1).name('splatRadiusMin')
      f.add(splat.normRadius, 'max', 0, 1, 0.1).name('splatRadiusMax')
    }
    compF.add(sys, 'colorByLifeTimeProgresser')
    compF.add(sys, 'colorBySpeedProgresser')
    {
      let f = compF.addFolder('lifetime/speed change')
      f.addColor(color.start, 'min').name('startColorMin').listen()
      f.add({ a: color.start.min[3] }, 'a', 0, 1, 0.05).name('alphaMin').onChange((val) => { color.start.min[3] = val })
      f.addColor(color.start, 'max').name('startColorMax').listen()
      f.add({ a: color.start.max[3] }, 'a', 0, 1, 0.05).name('alphaMax').onChange((val) => { color.start.max[3] = val })
      f.addColor(color.end, 'min').name('endColorMin').listen()
      f.add({ a: color.end.min[3] }, 'a', 0, 1, 0.05).name('alphaMin').onChange((val) => { color.end.min[3] = val })
      f.addColor(color.end, 'max').name('endColorMax').listen()
      f.add({ a: color.end.max[3] }, 'a', 0, 1, 0.05).name('alphaMax').onChange((val) => { color.end.max[3] = val })
    }
    compF.add(sys, 'colorModulationByLifeTimeProgresser')
    {
      let f = compF.addFolder('lifetime modulation')
      f.addColor(color.source, 'min').name('sourceColorMin').listen()
      f.add({ a: color.source.min[3] }, 'a', 0, 1, 0.05).name('alphaMin').onChange((val) => { color.source.min[3] = val })
      f.addColor(color.source, 'max').name('sourceColorMax').listen()
      f.add({ a: color.source.max[3] }, 'a', 0, 1, 0.05).name('alphaMax').onChange((val) => { color.source.max[3] = val })
      f.addColor(color.modulation, 'min').name('modColorMin').listen()
      f.add({ a: color.modulation.min[3] }, 'a', 0, 1, 0.05).name('alphaMin').onChange((val) => { color.modulation.min[3] = val })
      f.addColor(color.modulation, 'max').name('modColorMax').listen()
      f.add({ a: color.modulation.max[3] }, 'a', 0, 1, 0.05).name('alphaMax').onChange((val) => { color.modulation.max[3] = val })
    }
  }
  // Shape
  {
    let compF = gui.addFolder('Shape')
    let sys = particleSystem.systems
    let circle = particleSystem.componentInitializers.circleComponent
    let rect = particleSystem.componentInitializers.rectangleComponent
    // gui.remember(circle.radius)
    // gui.remember(rect.width)
    // gui.remember(rect.height)
    compF.add(sys, 'circleRenderer')
    compF.add(circle.radius, 'min').min(0).step(1).name('radiusMin')
    compF.add(circle.radius, 'max').min(0).step(1).name('radiusMax')
    compF.add(sys, 'rectangleRenderer')
    compF.add(rect.width, 'min').min(0).step(1).name('widthMin')
    compF.add(rect.width, 'max').min(0).step(1).name('widthMax')
    compF.add(rect.height, 'min').min(0).step(1).name('heightMin')
    compF.add(rect.height, 'max').min(0).step(1).name('heightMax')
  }
  // Scale
  {
    let compF = gui.addFolder('Scale')
    let sys = particleSystem.systems
    let scale = particleSystem.componentInitializers.scaleComponent
    // gui.remember(scale.scale)
    // gui.remember(scale.speed)
    // gui.remember(scale.start)
    // gui.remember(scale.end)
    // gui.remember(scale.min)
    // gui.remember(scale.max)
    compF.add(sys, 'scaleRenderer')
    compF.add(scale.scale, 'min').min(0).step(0.1).name('scaleMin')
    compF.add(scale.scale, 'max').min(0).step(0.1).name('scaleMax')
    compF.add(sys, 'scaleProgresser')
    compF.add(scale.speed, 'min', -0.01, 0.01, 0.00001).name('scaleSpeedMin')
    compF.add(scale.speed, 'max', -0.01, 0.01, 0.00001).name('scaleSpeedMax')
    compF.add(scale.min, 'min').min(0).step(0.1).name('scaleMinValMin')
    compF.add(scale.min, 'max').min(0).step(0.1).name('scaleMinValMax')
    compF.add(scale.max, 'min').min(0).step(0.1).name('scaleMaxValMin')
    compF.add(scale.max, 'max').min(0).step(0.1).name('scaleMaxValMax')
    compF.add(sys, 'scaleByLifeTimeProgresser')
    compF.add(sys, 'scaleBySpeedProgresser')
    compF.add(scale.start, 'min').min(0).step(0.1).name('scaleStartMin')
    compF.add(scale.start, 'max').min(0).step(0.1).name('scaleStartMax')
    compF.add(scale.end, 'min').min(0).step(0.1).name('scaleEndMin')
    compF.add(scale.end, 'max').min(0).step(0.1).name('scaleEndMax')
  }
  // Rotation
  {
    let compF = gui.addFolder('Rotation')
    let sys = particleSystem.systems
    let rotation = particleSystem.componentInitializers.rotationComponent
    // gui.remember(rotation.rotation)
    // gui.remember(rotation.speed)
    // gui.remember(rotation.start)
    // gui.remember(rotation.end)
    // gui.remember(rotation.min)
    // gui.remember(rotation.max)
    compF.add(sys, 'rotationRenderer')
    compF.add(rotation.rotation, 'min').min(0).step(0.01).name('rotationMin')
    compF.add(rotation.rotation, 'max').min(0).step(0.01).name('rotationMax')
    compF.add(sys, 'rotationProgresser')
    compF.add(rotation.speed, 'min', -0.01, 0.01, 0.00001).name('rotSpeedMin')
    compF.add(rotation.speed, 'max', -0.01, 0.01, 0.00001).name('rotSpeedMax')
    compF.add(rotation.min, 'min').min(0).step(0.01).name('rotMinValMin')
    compF.add(rotation.min, 'max').min(0).step(0.01).name('rotMinValMax')
    compF.add(rotation.max, 'min').min(0).step(0.01).name('rotMaxValMin')
    compF.add(rotation.max, 'max').min(0).step(0.01).name('rotMaxValMax')
    compF.add(sys, 'rotationByLifeTimeProgresser')
    compF.add(sys, 'rotationBySpeedProgresser')
    compF.add(rotation.start, 'min').min(0).step(0.01).name('rotStartMin')
    compF.add(rotation.start, 'max').min(0).step(0.01).name('rotStartMax')
    compF.add(rotation.end, 'min').min(0).step(0.01).name('rotEndMin')
    compF.add(rotation.end, 'max').min(0).step(0.01).name('rotEndMax')
  }
  // Linear Motion
  {
    let compF = gui.addFolder('Linear Motion')
    let sys = particleSystem.systems
    let velocity = particleSystem.componentInitializers.velocityComponent
    // gui.remember(velocity.direction)
    // gui.remember(velocity.speed)
    // gui.remember(velocity.acceleration)
    // gui.remember(velocity.start)
    // gui.remember(velocity.end)
    // gui.remember(velocity.min)
    // gui.remember(velocity.max)
    compF.add(sys, 'linearMotionProgresser')
    compF.add(velocity.direction, 'min', 0, 2 * Math.PI, 0.01).name('directionMin')
    compF.add(velocity.direction, 'max', 0, 2 * Math.PI, 0.01).name('directionMax')
    compF.add(velocity.speed, 'min', -2, 2, 0.01).name('speedMin')
    compF.add(velocity.speed, 'max', -2, 2, 0.01).name('speedMax')
    compF.add(velocity.acceleration, 'min', -0.01, 0.01, 0.00001).name('accelMin')
    compF.add(velocity.acceleration, 'max', -0.01, 0.01, 0.00001).name('accelMax')
    {
      let f = compF.addFolder('speed limits')
      f.add(velocity.min, 'min', -0.01, 0.01, 0.00001).name('minSpeedMin')
      f.add(velocity.min, 'max', -0.01, 0.01, 0.00001).name('minSpeedMax')
      f.add(velocity.max, 'min', -0.01, 0.01, 0.00001).name('maxSpeedMin')
      f.add(velocity.max, 'max', -0.01, 0.01, 0.00001).name('maxSpeedMax') }
    {
      let f = compF.addFolder('start/end (..BySpeed)')
      f.add(velocity.start, 'min', -1, 1, 0.01).name('startSpeedMin')
      f.add(velocity.start, 'max', -1, 1, 0.01).name('startSpeedMax')
      f.add(velocity.end, 'min', -1, 1, 0.01).name('endSpeedMin')
      f.add(velocity.end, 'max', -1, 1, 0.01).name('endSpeedMax') }
  }
  // Ring Motion
  {
    let compF = gui.addFolder('Ring Motion')
    let sys = particleSystem.systems
    let ringMotion = particleSystem.componentInitializers.ringMotionComponent
    // gui.remember(ringMotion.angularSpeed)
    // gui.remember(ringMotion.angularAcceleration)
    // gui.remember(ringMotion.ringRadius)
    // gui.remember(ringMotion.ringSpeed)
    compF.add(sys, 'ringMotionProgresser')
    compF.add(ringMotion.angularSpeed, 'min', -0.01, 0.01, 0.00001).name('angSpeedMin')
    compF.add(ringMotion.angularSpeed, 'max', -0.01, 0.01, 0.00001).name('angSpeedMax')
    compF.add(ringMotion.angularAcceleration, 'min', -0.01, 0.01, 0.00001).name('angAccelMin')
    compF.add(ringMotion.angularAcceleration, 'max', -0.01, 0.01, 0.00001).name('angAccelMax')
    compF.add(ringMotion.ringRadius, 'min').min(0).step(1).name('ringRadiusMin')
    compF.add(ringMotion.ringRadius, 'max').min(0).step(1).name('ringRadiusMax')
    compF.add(ringMotion.ringSpeed, 'min', -0.01, 0.01, 0.00001).name('ringSpeedMin')
    compF.add(ringMotion.ringSpeed, 'max', -0.01, 0.01, 0.00001).name('ringSpeedMax')
  }
  // Wave Motion
  {
    let compF = gui.addFolder('Wave Motion')
    let sys = particleSystem.systems
    let waveMotion = particleSystem.componentInitializers.waveMotionComponent
    // gui.remember(waveMotion.amplitude)
    // gui.remember(waveMotion.amplitudeSpeed)
    // gui.remember(waveMotion.frequency)
    // gui.remember(waveMotion.frequencySpeed)
    // gui.remember(waveMotion.rotation)
    // gui.remember(waveMotion.rotationSpeed)
    compF.add(sys, 'waveMotionProgresser')
    compF.add(waveMotion.amplitude, 'min').min(0).step(1).name('amplitudeMin')
    compF.add(waveMotion.amplitude, 'max').min(0).step(1).name('amplitudeMax')
    compF.add(waveMotion.amplitudeSpeed, 'min', -0.01, 0.01, 0.00001).name('ampSpeedMin')
    compF.add(waveMotion.amplitudeSpeed, 'max', -0.01, 0.01, 0.00001).name('ampSpeedMax')
    compF.add(waveMotion.frequency, 'min', -0.1, 0.1, 0.001).name('frequencyMin')
    compF.add(waveMotion.frequency, 'max', -0.1, 0.1, 0.001).name('frequencyMax')
    compF.add(waveMotion.frequencySpeed, 'min', -0.01, 0.01, 0.00001).name('freqSpeedMin')
    compF.add(waveMotion.frequencySpeed, 'max', -0.01, 0.01, 0.00001).name('freqSpeedMax')
    compF.add(waveMotion.rotation, 'min', 0, 2 * Math.PI, 0.01).name('rotationMin')
    compF.add(waveMotion.rotation, 'max', 0, 2 * Math.PI, 0.01).name('rotationMax')
    compF.add(waveMotion.rotationSpeed, 'min', -0.01, 0.01, 0.00001).name('rotSpeedMin')
    compF.add(waveMotion.rotationSpeed, 'max', -0.01, 0.01, 0.00001).name('rotSpeedMax')
  }
}

class _GUI {
  Initialize (_particleSystemManager) {
    this.guis = {}
    this.particleSystemManager = _particleSystemManager
    this.id = ''
    this.x = 300
    this.y = 300
    this.width = 100
    this.height = 60
    this.radius = 10
    this.preset = 'default'
    this.addId = '_new'
    // this.jsonObj = null

    this.fileInput = document.createElement('input')
    this.fileInput.type = 'file'
    this.fileInput.accept = 'application/json'
    this.fileInput.multiple = false
    this.loadButton = document.createElement('input')
    this.loadButton.type = 'button'
    this.loadButton.id = '_loadButton'
    this.loadButton.value = 'Load JSON'
    this.loadButton.addEventListener('click', () => {
      this.LoadJSON()
    })
    this.saveButton = document.createElement('input')
    this.saveButton.type = 'button'
    this.saveButton.id = '_saveButton'
    this.saveButton.value = 'Save JSON'
    this.saveButton.addEventListener('click', () => {
      this.SaveJSON()
    })

    let elDiv = document.getElementById('_elDiv')
    elDiv.appendChild(this.fileInput)
    elDiv.appendChild(this.loadButton)
    elDiv.appendChild(this.saveButton)
  }
  AddButton () {
    let [, ps] = this.particleSystemManager.Find(this.addId)
    if (ps) {
      console.log('Particle System already added')
      return
    }
    [, ps] = this.particleSystemManager.AddButton(this.x, this.y, this.width, this.height, this.addId, this.addId, this.preset)
    this.id = ps.id
    BuildParticleSystemGUI(this, ps)
  }
  AddRange () {
    let [, ps] = this.particleSystemManager.Find(this.addId)
    if (ps) {
      console.log('Particle System already added')
      return
    }
    [, ps] = this.particleSystemManager.AddRange(this.x, this.y, this.width, this.height, this.addId, this.preset)
    this.id = ps.id
    BuildParticleSystemGUI(this, ps)
  }
  AddNumber () {
    let [, ps] = this.particleSystemManager.Find(this.addId)
    if (ps) {
      console.log('Particle System already added')
      return
    }
    [, ps] = this.particleSystemManager.AddNumber(this.x, this.y, this.width, this.height, this.addId, this.preset)
    this.id = ps.id
    BuildParticleSystemGUI(this, ps)
  }
  AddCheckbox () {
    let [, ps] = this.particleSystemManager.Find(this.addId)
    if (ps) {
      console.log('Particle System already added')
      return
    }
    [, ps] = this.particleSystemManager.AddCheckbox(this.x, this.y, this.width, this.height, this.addId, this.preset)
    this.id = ps.id
    BuildParticleSystemGUI(this, ps)
  }
  AddRadio () {
    let [, ps] = this.particleSystemManager.Find(this.addId)
    if (ps) {
      console.log('Particle System already added')
      return
    }
    [, ps] = this.particleSystemManager.AddRadio(this.x, this.y, this.radius, this.addId, this.preset)
    this.id = ps.id
    BuildParticleSystemGUI(this, ps)
  }
  RemoveParticleSystem () {
    let rid = this.id
    if (!rid) return
    this.particleSystemManager.Remove(rid)
    console.assert(typeof this.guis[rid] !== 'undefined', 'RemoveSystem: GUI does not exist, id: ' + rid)
    this.guis[rid].destroy()
    delete this.guis[rid]
    let keys = Object.keys(this.particleSystemManager.GetParticleSystems())
    this.id = keys[0] || ''
  }
  MakeClickable () {
    if (!this.id) return
    let [el, ps] = this.particleSystemManager.Find(this.id)
    Clickable(el, ps)
  }
  MakeDraggable () {
    if (!this.id) return
    let [el, ps] = this.particleSystemManager.Find(this.id)
    Draggable(el, ps)
  }
  AddPointEmitShape () {
    if (!this.id) return
    let [, ps] = this.particleSystemManager.Find(this.id)
    let shape = new PointEmitShape(this.x, this.y)
    ps.emitArea.InsertEnd(shape)
  }
  AddLineEmitShape () {
    if (!this.id) return
    let [, ps] = this.particleSystemManager.Find(this.id)
    let shape = new LineEmitShape(this.x, this.y, this.x + this.width, this.y + this.height)
    ps.emitArea.InsertEnd(shape)
  }
  AddRectEmitShape () {
    if (!this.id) return
    let [, ps] = this.particleSystemManager.Find(this.id)
    let shape = new RectEmitShape(this.x, this.y, this.x + this.width, this.y + this.height, true)
    ps.emitArea.InsertEnd(shape)
  }
  AddCircleEmitShape () {
    if (!this.id) return
    let [, ps] = this.particleSystemManager.Find(this.id)
    let shape = new CircleEmitShape(this.x, this.y, this.radius, true)
    ps.emitArea.InsertEnd(shape)
  }

  SaveJSON () {
    let json = this.particleSystemManager.SaveState()

    let blob = new Blob([json], { type: 'application/json' })
    let url = URL.createObjectURL(blob)

    let a = document.createElement('a')
    a.id = '_download'
    a.download = 'state.json'
    a.textContent = 'Download state.json'
    a.target = '_blank'
    a.href = url

    let elDiv = document.getElementById('_elDiv')
    let oldA = document.getElementById('_download')
    if (oldA) elDiv.removeChild(oldA)
    elDiv.appendChild(a)
  }

  LoadJSON () {
    let files = this.fileInput.files
    // console.log(files)
    if (files.length <= 0) return

    let fr = new FileReader()
    fr.addEventListener('load', (e) => {
      let json = e.target.result
      // console.log(json)
      this.particleSystemManager.LoadState(json)
      Object.keys(this.guis).forEach((key) => {
        this.guis[key].destroy()
        delete this.guis[key]
      })
      this.BuildAll()
    })
    fr.readAsText(files.item(0))
  }

  BuildAll () {
    // Element GUI
    let opt = Object.keys(this.particleSystemManager.GetParticleSystems())
    let elementGui = new dat.GUI({ name: 'ElementGui' })
    this.guis[elementGui.name] = elementGui
    let keys = Object.keys(this.particleSystemManager.GetParticleSystems())
    this.id = keys[0] || ''
    let idContr = elementGui.add(this, 'id', opt).listen()
    elementGui.add(this, 'x').min(0).step(1)
    elementGui.add(this, 'y').min(0).step(1)
    elementGui.add(this, 'width').min(0).step(1)
    elementGui.add(this, 'height').min(0).step(1)
    elementGui.add(this, 'radius').min(0).step(1)
    // elementGui.add(this, 'preset', ['rain', 'explosion', 'ringwave'])
    let UpdateDropdown = (val) => {
      let keys = Object.keys(this.particleSystemManager.GetParticleSystems())
      idContr = idContr.options(keys)
    }
    let UpdateEmitShapes = (val) => {
      if (!this.id) return
      let [, ps] = this.particleSystemManager.Find(this.id)
      let shapes = ps.emitArea.GetShapes()
      let index = shapes.length - 1
      let topGui = this.guis[this.id]
      let areaF = topGui.__folders['emitArea']
      // update shape, side controllers
      let shapeC = topGui.__controllers[1]
      let sideC = topGui.__controllers[2]
      shapeC.setValue(-2)
      sideC.setValue(-2)
      shapeC.max(index)
      AddEmitShapeGUI(topGui, areaF, index, shapes[index])
      topGui.updateDisplay()
    }
    elementGui.add(this, 'RemoveParticleSystem').name('RemovePartSystem').onFinishChange(UpdateDropdown)
    elementGui.add(this, 'MakeClickable')
    elementGui.add(this, 'MakeDraggable')
    let psFolder = elementGui.addFolder('AddParticleSystems')
    psFolder.add(this, 'addId').listen().onFinishChange((val) => { if (!val) this.addId = '_new' })
    psFolder.add(this, 'AddButton').onFinishChange(UpdateDropdown)
    psFolder.add(this, 'AddRange').onFinishChange(UpdateDropdown)
    psFolder.add(this, 'AddNumber').onFinishChange(UpdateDropdown)
    psFolder.add(this, 'AddCheckbox').onFinishChange(UpdateDropdown)
    psFolder.add(this, 'AddRadio').onFinishChange(UpdateDropdown)
    let shapeFolder = elementGui.addFolder('Add EmitShapes')
    shapeFolder.add(this, 'AddPointEmitShape').name('AddPointEShape').onFinishChange(UpdateEmitShapes)
    shapeFolder.add(this, 'AddLineEmitShape').name('AddLineEShape').onFinishChange(UpdateEmitShapes)
    shapeFolder.add(this, 'AddRectEmitShape').name('AddRectEShape').onFinishChange(UpdateEmitShapes)
    shapeFolder.add(this, 'AddCircleEmitShape').name('AddCircleEShape').onFinishChange(UpdateEmitShapes)
    elementGui.add(this.particleSystemManager, 'displayFps')
    // Particle Systems GUIs
    Object.values(this.particleSystemManager.GetParticleSystems()).reverse().forEach((particleSystem) => {
      BuildParticleSystemGUI(this, particleSystem)
    })
  }
}
export const GUI = new _GUI()
