# JFX

Particle effects library in JavaScript for Web applications

## Run Instructions

- Install _Live Server_ plugin on vscode
- Open _JFX_ folder in vscode
- Run with _Go Live_ button

## JSON Files
The state of all particle systems can be saved to a JSON file and then loaded on demand.

Use the Save\Load JSON panel on the top left.

## Property flag values
Particle System

spawnShape
- -2: random point from all emit area shapes
- -1: next shape on each particle spawn

spawnSide
- -2: random point from all emit area shapes
- -1: next side on each particle spawn

Spawner Component

maxTank
- 0: tank is not utilized

spawnRate
- 0: whole tank is spawned

spawnDelay
- < 0: spawner disabled
- 0: instant spawn (used on click/drag events)

For combination of ring and wave motion, wave motion must be applied after ring motion

No limits are applied if all are set to zero

Developed in chrome browser

dat.GUI used for live editor

https://github.com/dataarts/dat.gui
