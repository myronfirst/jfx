import { EmitArea } from './EmitArea.js'
import { PointEmitShape, LineEmitShape, RectEmitShape, CircleEmitShape } from './EmitShapes.js'
import { CSManager } from './CSManager.js'
import { InitRange } from './Utilities.js'
import { ParticleSystem } from './ParticleSystem.js'
import { Rect } from './Shapes.js'
import { Clickable, Draggable } from './EventListeners.js'

function GetElementEmitArea (el) {
  let domRect = el.getBoundingClientRect()
  let emitArea = {}
  switch (el.type.toLowerCase()) {
    case 'button':
      emitArea = new EmitArea([new RectEmitShape(domRect.left, domRect.top, domRect.right, domRect.bottom, true)])
      break
    case 'range':
      let x1 = domRect.left
      let x2 = domRect.right
      let y = domRect.top + domRect.height / 2
      emitArea = new EmitArea([new LineEmitShape(x1, y, x2, y, true)])
      break
    case 'number':
      emitArea = new EmitArea([new RectEmitShape(domRect.left, domRect.top, domRect.right, domRect.bottom, true)])
      break
    case 'checkbox':
      emitArea = new EmitArea([new RectEmitShape(domRect.left, domRect.top, domRect.right, domRect.bottom, true)])
      break
    case 'radio':
      let rect = new Rect(domRect.left, domRect.top, domRect.right, domRect.bottom)
      let center = rect.GetCenter()
      let radius = (rect.GetWidth() + rect.GetHeight()) / 4
      emitArea = new EmitArea([new CircleEmitShape(center.x, center.y, radius, true)])
      break
    default:
      emitArea = new EmitArea([new RectEmitShape(domRect.left, domRect.top, domRect.right, domRect.bottom, true)])
      break
  }
  return emitArea
}

function DefaultPreset () {
  let relativeEmitDirection = 'none'
  let spawnerInitializer = CSManager.GetInitializer('spawnerComponent')
  spawnerInitializer.maxTank.Set(0)
  spawnerInitializer.tank.Set(0)
  spawnerInitializer.maxAlive.Set(100)
  spawnerInitializer.spawnRate.Set(10)
  spawnerInitializer.spawnDelay.Set(1 * 1000)

  let initializers = {
    colorComponent: CSManager.GetInitializer('colorComponent'),
    splatComponent: CSManager.GetInitializer('splatComponent'),
    scaleComponent: CSManager.GetInitializer('scaleComponent'),
    rotationComponent: CSManager.GetInitializer('rotationComponent'),
    velocityComponent: CSManager.GetInitializer('velocityComponent'),
    ringMotionComponent: CSManager.GetInitializer('ringMotionComponent'),
    waveMotionComponent: CSManager.GetInitializer('waveMotionComponent'),
    circleComponent: CSManager.GetInitializer('circleComponent'),
    rectangleComponent: CSManager.GetInitializer('rectangleComponent')
  }

  let lifeTime = new InitRange(3 * 1000)

  initializers.circleComponent.radius.Set(8)

  initializers.velocityComponent.speed.Set(0.1, 0.3)
  initializers.velocityComponent.acceleration.Set(0)
  initializers.velocityComponent.direction.Set(Math.PI / 2 - 0.3, Math.PI / 2 + 0.3)
  initializers.velocityComponent.start.Set(0.1)
  initializers.velocityComponent.end.Set(0.3)
  initializers.velocityComponent.min.Set(0)
  initializers.velocityComponent.max.Set(0)

  initializers.colorComponent.color.Set([0, 0, 0, 1])

  initializers.splatComponent.backColor.Set([255, 255, 255, 0])
  initializers.splatComponent.normRadius.Set(0.4)

  initializers.scaleComponent.start.Set(1)
  initializers.scaleComponent.end.Set(0)

  let systems = {
    scaleProgresser: false,
    rotationProgresser: false,
    colorByLifeTimeProgresser: false,
    colorBySpeedProgresser: false,
    colorModulationByLifeTimeProgresser: false,
    scaleByLifeTimeProgresser: true,
    scaleBySpeedProgresser: false,
    rotationByLifeTimeProgresser: false,
    rotationBySpeedProgresser: false,
    linearMotionProgresser: true,
    ringMotionProgresser: false,
    waveMotionProgresser: false,

    colorRenderer: false,
    splatRenderer: true,
    scaleRenderer: true,
    rotationRenderer: false,
    circleRenderer: true,
    rectangleRenderer: false
  }

  return {
    lifeTime: lifeTime,
    relativeEmitDirection: relativeEmitDirection,
    spawnerInitializer: spawnerInitializer,
    initializers: initializers,
    systems: systems
  }
}
function RainPreset () {
  let relativeEmitDirection = 'none'
  let spawnerInitializer = CSManager.GetInitializer('spawnerComponent')
  spawnerInitializer.maxTank.Set(0)
  spawnerInitializer.tank.Set(0)
  spawnerInitializer.maxAlive.Set(100)
  spawnerInitializer.spawnRate.Set(5)
  spawnerInitializer.spawnDelay.Set(1 * 1000)

  let initializers = {
    colorComponent: CSManager.GetInitializer('colorComponent'),
    splatComponent: CSManager.GetInitializer('splatComponent'),
    scaleComponent: CSManager.GetInitializer('scaleComponent'),
    rotationComponent: CSManager.GetInitializer('rotationComponent'),
    velocityComponent: CSManager.GetInitializer('velocityComponent'),
    ringMotionComponent: CSManager.GetInitializer('ringMotionComponent'),
    waveMotionComponent: CSManager.GetInitializer('waveMotionComponent'),
    circleComponent: CSManager.GetInitializer('circleComponent'),
    rectangleComponent: CSManager.GetInitializer('rectangleComponent')
  }

  let lifeTime = new InitRange(3 * 1000)

  initializers.circleComponent.radius.Set(10)
  initializers.rectangleComponent.width.Set(10)
  initializers.rectangleComponent.height.Set(5)

  initializers.velocityComponent.speed.Set(0.1, 0.2)
  initializers.velocityComponent.acceleration.Set(0.0002)
  initializers.velocityComponent.direction.Set(Math.PI / 2 - 0.2, Math.PI / 2 + 0.2)
  initializers.velocityComponent.start.Set(0.1)
  initializers.velocityComponent.end.Set(0.2)
  initializers.velocityComponent.min.Set(0)
  initializers.velocityComponent.max.Set(0.4)

  initializers.colorComponent.start.Set([161, 3, 252, 1])

  initializers.colorComponent.end.Set([3, 144, 252, 0.8])

  initializers.splatComponent.backColor.Set([255, 255, 255, 0])
  initializers.splatComponent.normRadius.Set(0.6)

  initializers.scaleComponent.start.Set(1)
  initializers.scaleComponent.end.Set(0)
  initializers.scaleComponent.scale.Set(2)
  initializers.scaleComponent.speed.Set(-0.001)
  initializers.scaleComponent.min.Set(1)
  initializers.scaleComponent.max.Set(2)

  initializers.rotationComponent.rotation.Set(6 * Math.PI)
  initializers.rotationComponent.speed.Set(-0.02)
  initializers.rotationComponent.min.Set(0)
  initializers.rotationComponent.max.Set(4 * Math.PI)

  let systems = {
    scaleProgresser: false,
    rotationProgresser: false,
    colorByLifeTimeProgresser: false,
    colorBySpeedProgresser: true,
    colorModulationByLifeTimeProgresser: false,
    scaleByLifeTimeProgresser: true,
    scaleBySpeedProgresser: false,
    rotationByLifeTimeProgresser: false,
    rotationBySpeedProgresser: false,
    linearMotionProgresser: true,
    ringMotionProgresser: false,
    waveMotionProgresser: false,

    colorRenderer: false,
    splatRenderer: true,
    scaleRenderer: true,
    rotationRenderer: true,
    circleRenderer: true,
    rectangleRenderer: false
  }

  return {
    lifeTime: lifeTime,
    relativeEmitDirection: relativeEmitDirection,
    spawnerInitializer: spawnerInitializer,
    initializers: initializers,
    systems: systems
  }
}
function ExplosionPreset () {
  let relativeEmitDirection = 'centerout'
  let spawnerInitializer = CSManager.GetInitializer('spawnerComponent')
  spawnerInitializer.maxTank.Set(0)
  spawnerInitializer.tank.Set(0)
  spawnerInitializer.maxAlive.Set(300)
  spawnerInitializer.spawnRate.Set(60)
  spawnerInitializer.spawnDelay.Set(1 * 1000)

  let initializers = {
    colorComponent: CSManager.GetInitializer('colorComponent'),
    splatComponent: CSManager.GetInitializer('splatComponent'),
    scaleComponent: CSManager.GetInitializer('scaleComponent'),
    rotationComponent: CSManager.GetInitializer('rotationComponent'),
    velocityComponent: CSManager.GetInitializer('velocityComponent'),
    ringMotionComponent: CSManager.GetInitializer('ringMotionComponent'),
    waveMotionComponent: CSManager.GetInitializer('waveMotionComponent'),
    circleComponent: CSManager.GetInitializer('circleComponent'),
    rectangleComponent: CSManager.GetInitializer('rectangleComponent')
  }
  let lifeTime = new InitRange(2 * 1000)

  initializers.rectangleComponent.width.Set(16)
  initializers.rectangleComponent.height.Set(12)

  initializers.velocityComponent.speed.Set(0.1, 0.3)
  initializers.velocityComponent.acceleration.Set(-0.00001)
  initializers.velocityComponent.direction.Set(0, 2 * Math.PI)
  initializers.velocityComponent.start.Set(0.1)
  initializers.velocityComponent.end.Set(0.3)
  initializers.velocityComponent.min.Set(0)
  initializers.velocityComponent.max.Set(0.5)

  initializers.colorComponent.start.Set([3, 252, 19, 1])

  initializers.colorComponent.end.Set([98, 3, 252, 1])

  initializers.splatComponent.backColor.Set([255, 255, 255, 0])
  initializers.splatComponent.normRadius.Set(0.1)

  initializers.scaleComponent.scale.Set(1)
  initializers.scaleComponent.start.Set(0)
  initializers.scaleComponent.end.Set(1)

  initializers.rotationComponent.rotation.Set(0)
  initializers.rotationComponent.start.Set(0)
  initializers.rotationComponent.end.Set(2 * Math.PI)

  let systems = {
    scaleProgresser: true,
    rotationProgresser: true,
    colorByLifeTimeProgresser: true,
    colorBySpeedProgresser: false,
    colorModulationByLifeTimeProgresser: false,
    scaleByLifeTimeProgresser: false,
    scaleBySpeedProgresser: true,
    rotationByLifeTimeProgresser: false,
    rotationBySpeedProgresser: true,
    linearMotionProgresser: true,
    ringMotionProgresser: false,
    waveMotionProgresser: false,

    colorRenderer: false,
    splatRenderer: true,
    scaleRenderer: true,
    rotationRenderer: true,
    circleRenderer: false,
    rectangleRenderer: true
  }

  return {
    lifeTime: lifeTime,
    relativeEmitDirection: relativeEmitDirection,
    spawnerInitializer: spawnerInitializer,
    initializers: initializers,
    systems: systems
  }
}
function RingWavePreset () {
  let relativeEmitDirection = 'none'
  let spawnerInitializer = CSManager.GetInitializer('spawnerComponent')
  spawnerInitializer.maxTank.Set(0)
  spawnerInitializer.tank.Set(0)
  spawnerInitializer.maxAlive.Set(100)
  spawnerInitializer.spawnRate.Set(1)
  spawnerInitializer.spawnDelay.Set(0.2 * 1000)

  let initializers = {
    colorComponent: CSManager.GetInitializer('colorComponent'),
    splatComponent: CSManager.GetInitializer('splatComponent'),
    scaleComponent: CSManager.GetInitializer('scaleComponent'),
    rotationComponent: CSManager.GetInitializer('rotationComponent'),
    velocityComponent: CSManager.GetInitializer('velocityComponent'),
    ringMotionComponent: CSManager.GetInitializer('ringMotionComponent'),
    waveMotionComponent: CSManager.GetInitializer('waveMotionComponent'),
    circleComponent: CSManager.GetInitializer('circleComponent'),
    rectangleComponent: CSManager.GetInitializer('rectangleComponent')
  }

  let lifeTime = new InitRange(6 * 1000)

  initializers.circleComponent.radius.Set(6)

  initializers.velocityComponent.speed.Set(0.1)
  initializers.velocityComponent.acceleration.Set(0)
  initializers.velocityComponent.direction.Set(0, 2 * Math.PI)
  initializers.velocityComponent.min.Set(0)
  initializers.velocityComponent.max.Set(0.4)

  initializers.colorComponent.start.Set([255, 3, 3, 1])
  // initializers.colorComponent.start.r.Set(255)
  // initializers.colorComponent.start.g.Set(3)
  // initializers.colorComponent.start.b.Set(3)
  // initializers.colorComponent.start.a.Set(1)

  initializers.colorComponent.end.Set([255, 255, 3, 1])
  // initializers.colorComponent.end.r.Set(255)
  // initializers.colorComponent.end.g.Set(255)
  // initializers.colorComponent.end.b.Set(3)
  // initializers.colorComponent.end.a.Set(1)

  initializers.ringMotionComponent.angularSpeed.Set(0.01)
  initializers.ringMotionComponent.ringRadius.Set(3)

  initializers.waveMotionComponent.amplitude.Set(2)
  initializers.waveMotionComponent.frequency.Set(0.06)
  initializers.waveMotionComponent.rotationSpeed.Set(0.001)

  let systems = {
    scaleProgresser: false,
    rotationProgresser: false,
    colorByLifeTimeProgresser: true,
    colorBySpeedProgresser: false,
    colorModulationByLifeTimeProgresser: false,
    scaleByLifeTimeProgresser: false,
    scaleBySpeedProgresser: false,
    rotationByLifeTimeProgresser: false,
    rotationBySpeedProgresser: false,
    linearMotionProgresser: true,
    ringMotionProgresser: true,
    waveMotionProgresser: true,

    colorRenderer: true,
    splatRenderer: false,
    scaleRenderer: false,
    rotationRenderer: false,
    circleRenderer: true,
    rectangleRenderer: false
  }

  return {
    lifeTime: lifeTime,
    relativeEmitDirection: relativeEmitDirection,
    spawnerInitializer: spawnerInitializer,
    initializers: initializers,
    systems: systems
  }
}
function GetPreset (preset) {
  switch (preset) {
    case 'explosion':
      return ExplosionPreset()
    case 'ringwave':
      return RingWavePreset()
    case 'rain':
      return RainPreset()
    case 'default': default:
      return DefaultPreset()
  }
}

function RestoreState (manager, state) {
  // remove DOM children
  // while (manager.elDiv.firstChild) {
  //   manager.elDiv.removeChild(manager.elDiv.firstChild)
  // }
  // manager.elDiv.appendChild(manager.canvas)
  Object.entries(manager.elements).forEach(([id, el]) => {
    manager.elDiv.removeChild(el)
  })
  manager.elements = {}
  manager.particleSystems = {}
  Object.entries(state.particleSystems).forEach(([id, ps]) => {
    // remove old element
    let oldEl = document.getElementById(id)
    if (oldEl) manager.elDiv.removeChild(oldEl)
    // restore dom element props
    let el = state.elements[id]
    console.assert(typeof el !== 'undefined', 'el not found in state')
    let nel = document.createElement('input')
    Object.assign(nel, el)
    Object.assign(nel.style, el.style)
    nel.clickFlag = el.clickFlag
    nel.dragFlag = el.dragFlag
    manager.elDiv.appendChild(nel)
    // restore particle system props, componentInitializers, systems
    let spawnerInit = CSManager.GetInitializer('spawnerComponent')
    let nps = new ParticleSystem(null, {}, null, null, null, null, null, spawnerInit, {}, {})
    nps.id = ps.id
    nps.lastTime = ps.lastTime
    nps.aliveParticles = []
    nps.deadParticles = []
    // restore emitArea
    nps.emitArea = new EmitArea(ps.emitArea.emitShapesArray.slice(0))
    nps.emitArea.index = ps.emitArea.index
    ps.emitArea.emitShapesArray.forEach((shape, i) => {
      if (shape.hasOwnProperty('point')) {
        nps.emitArea.emitShapesArray[i] = new PointEmitShape(shape.point.x, shape.point.y)
      } else if (shape.hasOwnProperty('line')) {
        nps.emitArea.emitShapesArray[i] = new LineEmitShape(shape.line.x1, shape.line.y1, shape.line.x2, shape.line.y2)
      } else if (shape.hasOwnProperty('rect')) {
        nps.emitArea.emitShapesArray[i] = new RectEmitShape(shape.rect.x1, shape.rect.y1, shape.rect.x2, shape.rect.y2, shape.edgeEmit)
      } else if (shape.hasOwnProperty('circle')) {
        nps.emitArea.emitShapesArray[i] = new CircleEmitShape(shape.circle.x, shape.circle.y, shape.circle.radius, shape.edgeEmit)
      } else {
        console.assert('RestoreState: missing if case')
      }
    })

    nps.displayEmitArea = ps.displayEmitArea
    nps.spawnShape = ps.spawnShape
    nps.spawnSide = ps.spawnSide
    nps.lifeTime = new InitRange(ps.lifeTime.min, ps.lifeTime.max)
    nps.relativeEmitDirection = ps.relativeEmitDirection

    // restore spawner component
    nps.spawnerComponent = CSManager.GetComponent('spawnerComponent')
    nps.spawnerComponent.maxTank = ps.spawnerComponent.maxTank
    nps.spawnerComponent.maxAlive = ps.spawnerComponent.maxAlive
    nps.spawnerComponent.spawnRate = ps.spawnerComponent.spawnRate
    nps.spawnerComponent.spawnRate = ps.spawnerComponent.spawnRate
    nps.spawnerComponent.spawnDelay = ps.spawnerComponent.spawnDelay
    nps.spawnerComponent.tank = ps.spawnerComponent.tank
    nps.spawnerComponent.lastTime = ps.spawnerComponent.lastTime

    // restore componentInitializers
    nps.componentInitializers.colorComponent = CSManager.GetInitializer('colorComponent')
    nps.componentInitializers.colorComponent.color.min = ps.componentInitializers.colorComponent.color.min.slice(0)
    nps.componentInitializers.colorComponent.color.max = ps.componentInitializers.colorComponent.color.max.slice(0)
    nps.componentInitializers.colorComponent.start.min = ps.componentInitializers.colorComponent.start.min.slice(0)
    nps.componentInitializers.colorComponent.start.max = ps.componentInitializers.colorComponent.start.max.slice(0)
    nps.componentInitializers.colorComponent.end.min = ps.componentInitializers.colorComponent.end.min.slice(0)
    nps.componentInitializers.colorComponent.end.max = ps.componentInitializers.colorComponent.end.max.slice(0)
    nps.componentInitializers.colorComponent.source.min = ps.componentInitializers.colorComponent.source.min.slice(0)
    nps.componentInitializers.colorComponent.source.max = ps.componentInitializers.colorComponent.source.max.slice(0)
    nps.componentInitializers.colorComponent.modulation.min = ps.componentInitializers.colorComponent.modulation.min.slice(0)
    nps.componentInitializers.colorComponent.modulation.max = ps.componentInitializers.colorComponent.modulation.max.slice(0)

    nps.componentInitializers.splatComponent = CSManager.GetInitializer('splatComponent')
    nps.componentInitializers.splatComponent.backColor.min = ps.componentInitializers.splatComponent.backColor.min.slice(0)
    nps.componentInitializers.splatComponent.backColor.max = ps.componentInitializers.splatComponent.backColor.max.slice(0)
    nps.componentInitializers.splatComponent.normRadius.min = ps.componentInitializers.splatComponent.normRadius.min
    nps.componentInitializers.splatComponent.normRadius.max = ps.componentInitializers.splatComponent.normRadius.max

    nps.componentInitializers.scaleComponent = CSManager.GetInitializer('scaleComponent')
    nps.componentInitializers.scaleComponent.scale.min = ps.componentInitializers.scaleComponent.scale.min
    nps.componentInitializers.scaleComponent.scale.max = ps.componentInitializers.scaleComponent.scale.max
    nps.componentInitializers.scaleComponent.speed.min = ps.componentInitializers.scaleComponent.speed.min
    nps.componentInitializers.scaleComponent.speed.max = ps.componentInitializers.scaleComponent.speed.max
    nps.componentInitializers.scaleComponent.start.min = ps.componentInitializers.scaleComponent.start.min
    nps.componentInitializers.scaleComponent.start.max = ps.componentInitializers.scaleComponent.start.max
    nps.componentInitializers.scaleComponent.end.min = ps.componentInitializers.scaleComponent.end.min
    nps.componentInitializers.scaleComponent.end.max = ps.componentInitializers.scaleComponent.end.max
    nps.componentInitializers.scaleComponent.min.min = ps.componentInitializers.scaleComponent.min.min
    nps.componentInitializers.scaleComponent.min.max = ps.componentInitializers.scaleComponent.min.max
    nps.componentInitializers.scaleComponent.max.min = ps.componentInitializers.scaleComponent.max.min
    nps.componentInitializers.scaleComponent.max.max = ps.componentInitializers.scaleComponent.max.max

    nps.componentInitializers.rotationComponent = CSManager.GetInitializer('rotationComponent')
    nps.componentInitializers.rotationComponent.rotation.min = ps.componentInitializers.rotationComponent.rotation.min
    nps.componentInitializers.rotationComponent.rotation.max = ps.componentInitializers.rotationComponent.rotation.max
    nps.componentInitializers.rotationComponent.speed.min = ps.componentInitializers.rotationComponent.speed.min
    nps.componentInitializers.rotationComponent.speed.max = ps.componentInitializers.rotationComponent.speed.max
    nps.componentInitializers.rotationComponent.start.min = ps.componentInitializers.rotationComponent.start.min
    nps.componentInitializers.rotationComponent.start.max = ps.componentInitializers.rotationComponent.start.max
    nps.componentInitializers.rotationComponent.end.min = ps.componentInitializers.rotationComponent.end.min
    nps.componentInitializers.rotationComponent.end.max = ps.componentInitializers.rotationComponent.end.max
    nps.componentInitializers.rotationComponent.min.min = ps.componentInitializers.rotationComponent.min.min
    nps.componentInitializers.rotationComponent.min.max = ps.componentInitializers.rotationComponent.min.max
    nps.componentInitializers.rotationComponent.max.min = ps.componentInitializers.rotationComponent.max.min
    nps.componentInitializers.rotationComponent.max.max = ps.componentInitializers.rotationComponent.max.max

    nps.componentInitializers.velocityComponent = CSManager.GetInitializer('velocityComponent')
    nps.componentInitializers.velocityComponent.speed.min = ps.componentInitializers.velocityComponent.speed.min
    nps.componentInitializers.velocityComponent.speed.max = ps.componentInitializers.velocityComponent.speed.max
    nps.componentInitializers.velocityComponent.acceleration.min = ps.componentInitializers.velocityComponent.acceleration.min
    nps.componentInitializers.velocityComponent.acceleration.max = ps.componentInitializers.velocityComponent.acceleration.max
    nps.componentInitializers.velocityComponent.direction.min = ps.componentInitializers.velocityComponent.direction.min
    nps.componentInitializers.velocityComponent.direction.max = ps.componentInitializers.velocityComponent.direction.max
    nps.componentInitializers.velocityComponent.start.min = ps.componentInitializers.velocityComponent.start.min
    nps.componentInitializers.velocityComponent.start.max = ps.componentInitializers.velocityComponent.start.max
    nps.componentInitializers.velocityComponent.end.min = ps.componentInitializers.velocityComponent.end.min
    nps.componentInitializers.velocityComponent.end.max = ps.componentInitializers.velocityComponent.end.max
    nps.componentInitializers.velocityComponent.min.min = ps.componentInitializers.velocityComponent.min.min
    nps.componentInitializers.velocityComponent.min.max = ps.componentInitializers.velocityComponent.min.max
    nps.componentInitializers.velocityComponent.max.min = ps.componentInitializers.velocityComponent.max.min
    nps.componentInitializers.velocityComponent.max.max = ps.componentInitializers.velocityComponent.max.max

    nps.componentInitializers.ringMotionComponent = CSManager.GetInitializer('ringMotionComponent')
    nps.componentInitializers.ringMotionComponent.center.x = ps.componentInitializers.ringMotionComponent.center.x
    nps.componentInitializers.ringMotionComponent.center.y = ps.componentInitializers.ringMotionComponent.center.y
    nps.componentInitializers.ringMotionComponent.angle.min = ps.componentInitializers.ringMotionComponent.angle.min
    nps.componentInitializers.ringMotionComponent.angle.max = ps.componentInitializers.ringMotionComponent.angle.max
    nps.componentInitializers.ringMotionComponent.angularSpeed.min = ps.componentInitializers.ringMotionComponent.angularSpeed.min
    nps.componentInitializers.ringMotionComponent.angularSpeed.max = ps.componentInitializers.ringMotionComponent.angularSpeed.max
    nps.componentInitializers.ringMotionComponent.angularAcceleration.min = ps.componentInitializers.ringMotionComponent.angularAcceleration.min
    nps.componentInitializers.ringMotionComponent.angularAcceleration.max = ps.componentInitializers.ringMotionComponent.angularAcceleration.max
    nps.componentInitializers.ringMotionComponent.ringRadius.min = ps.componentInitializers.ringMotionComponent.ringRadius.min
    nps.componentInitializers.ringMotionComponent.ringRadius.max = ps.componentInitializers.ringMotionComponent.ringRadius.max
    nps.componentInitializers.ringMotionComponent.ringSpeed.min = ps.componentInitializers.ringMotionComponent.ringSpeed.min
    nps.componentInitializers.ringMotionComponent.ringSpeed.max = ps.componentInitializers.ringMotionComponent.ringSpeed.max

    nps.componentInitializers.waveMotionComponent = CSManager.GetInitializer('waveMotionComponent')
    nps.componentInitializers.waveMotionComponent.pos.min = ps.componentInitializers.waveMotionComponent.pos.min
    nps.componentInitializers.waveMotionComponent.pos.max = ps.componentInitializers.waveMotionComponent.pos.max
    nps.componentInitializers.waveMotionComponent.center.x = ps.componentInitializers.waveMotionComponent.center.x
    nps.componentInitializers.waveMotionComponent.center.y = ps.componentInitializers.waveMotionComponent.center.y
    nps.componentInitializers.waveMotionComponent.amplitude.min = ps.componentInitializers.waveMotionComponent.amplitude.min
    nps.componentInitializers.waveMotionComponent.amplitude.max = ps.componentInitializers.waveMotionComponent.amplitude.max
    nps.componentInitializers.waveMotionComponent.amplitudeSpeed.min = ps.componentInitializers.waveMotionComponent.amplitudeSpeed.min
    nps.componentInitializers.waveMotionComponent.amplitudeSpeed.max = ps.componentInitializers.waveMotionComponent.amplitudeSpeed.max
    nps.componentInitializers.waveMotionComponent.angle.min = ps.componentInitializers.waveMotionComponent.angle.min
    nps.componentInitializers.waveMotionComponent.angle.max = ps.componentInitializers.waveMotionComponent.angle.max
    nps.componentInitializers.waveMotionComponent.frequency.min = ps.componentInitializers.waveMotionComponent.frequency.min
    nps.componentInitializers.waveMotionComponent.frequency.max = ps.componentInitializers.waveMotionComponent.frequency.max
    nps.componentInitializers.waveMotionComponent.frequencySpeed.min = ps.componentInitializers.waveMotionComponent.frequencySpeed.min
    nps.componentInitializers.waveMotionComponent.frequencySpeed.max = ps.componentInitializers.waveMotionComponent.frequencySpeed.max
    nps.componentInitializers.waveMotionComponent.rotation.min = ps.componentInitializers.waveMotionComponent.rotation.min
    nps.componentInitializers.waveMotionComponent.rotation.max = ps.componentInitializers.waveMotionComponent.rotation.max
    nps.componentInitializers.waveMotionComponent.rotationSpeed.min = ps.componentInitializers.waveMotionComponent.rotationSpeed.min
    nps.componentInitializers.waveMotionComponent.rotationSpeed.max = ps.componentInitializers.waveMotionComponent.rotationSpeed.max

    nps.componentInitializers.circleComponent = CSManager.GetInitializer('circleComponent')
    nps.componentInitializers.circleComponent.radius.min = ps.componentInitializers.circleComponent.radius.min
    nps.componentInitializers.circleComponent.radius.max = ps.componentInitializers.circleComponent.radius.max

    nps.componentInitializers.rectangleComponent = CSManager.GetInitializer('rectangleComponent')
    nps.componentInitializers.rectangleComponent.width.min = ps.componentInitializers.rectangleComponent.width.min
    nps.componentInitializers.rectangleComponent.width.max = ps.componentInitializers.rectangleComponent.width.max
    nps.componentInitializers.rectangleComponent.height.min = ps.componentInitializers.rectangleComponent.height.min
    nps.componentInitializers.rectangleComponent.height.max = ps.componentInitializers.rectangleComponent.height.max

    // restore systems
    Object.keys(ps.systems).forEach((key) => {
      nps.systems[key] = ps.systems[key]
    })

    // Clickable Draggable
    if (nel.clickFlag) {
      nel.clickFlag = false
      Clickable(nel, nps)
    }
    if (nel.dragFlag) {
      nel.dragFlag = false
      Draggable(nel, nps)
    }

    manager.elements[id] = nel
    manager.particleSystems[id] = nps
  })
}

class _ParticleSystemManager {
  Initialize () {
    this.canvas = document.createElement('canvas')
    this.canvas.width = window.innerWidth
    this.canvas.height = window.innerHeight
    this.elDiv = document.createElement('div')
    this.elDiv.id = '_elDiv'

    this.lastFrameTime = 0
    this.fpsArray = []
    this.fpsAvg = 0
    this.displayFps = false
    this.elements = {}
    this.particleSystems = {}

    this.elDiv.appendChild(this.canvas)
    document.body.appendChild(this.elDiv)
  }
  AddButton (x, y, w, h, val, pid, preset) {
    console.assert(!this.particleSystems[pid], 'ParticleSystemManager: Particle System already exists, pid: ' + pid)
    let el = document.createElement('input')
    el.id = pid
    el.type = 'button'
    el.value = val
    el.style.position = 'absolute'
    el.style.left = x + 'px'
    el.style.top = y + 'px'
    el.style.width = w + 'px'
    el.style.height = h + 'px'
    this.elDiv.appendChild(el)
    let emitArea = GetElementEmitArea(el)
    let pr = GetPreset(preset)
    let particleSystem = new ParticleSystem(pid, emitArea, true, -2, -2, pr.lifeTime, pr.relativeEmitDirection, pr.spawnerInitializer, pr.initializers, pr.systems)
    this.elements[pid] = el
    this.particleSystems[pid] = particleSystem
    return [el, particleSystem]
  }
  AddRange (x, y, w, h, pid, preset) {
    console.assert(!this.particleSystems[pid], 'ParticleSystemManager: Particle System already exists, pid: ' + pid)
    let el = document.createElement('input')
    el.id = pid
    el.type = 'range'
    el.style.position = 'absolute'
    el.style.left = x + 'px'
    el.style.top = y + 'px'
    el.style.width = w + 'px'
    el.style.height = h + 'px'
    this.elDiv.appendChild(el)
    let emitArea = GetElementEmitArea(el)
    let pr = GetPreset(preset)
    let particleSystem = new ParticleSystem(pid, emitArea, true, -2, -2, pr.lifeTime, pr.relativeEmitDirection, pr.spawnerInitializer, pr.initializers, pr.systems)
    this.elements[pid] = el
    this.particleSystems[pid] = particleSystem
    return [el, particleSystem]
  }
  AddNumber (x, y, w, h, pid, preset) {
    console.assert(!this.particleSystems[pid], 'ParticleSystemManager: Particle System already exists, pid: ' + pid)
    let el = document.createElement('input')
    el.id = pid
    el.type = 'number'
    el.style.position = 'absolute'
    el.style.left = x + 'px'
    el.style.top = y + 'px'
    el.style.width = w + 'px'
    el.style.height = h + 'px'
    this.elDiv.appendChild(el)
    let emitArea = GetElementEmitArea(el)
    let pr = GetPreset(preset)
    let particleSystem = new ParticleSystem(pid, emitArea, true, -2, -2, pr.lifeTime, pr.relativeEmitDirection, pr.spawnerInitializer, pr.initializers, pr.systems)
    this.elements[pid] = el
    this.particleSystems[pid] = particleSystem
    return [el, particleSystem]
  }
  AddCheckbox (x, y, w, h, pid, preset) {
    console.assert(!this.particleSystems[pid], 'ParticleSystemManager: Particle System already exists, pid: ' + pid)
    let el = document.createElement('input')
    el.id = pid
    el.type = 'checkbox'
    el.style.position = 'absolute'
    el.style.left = x + 'px'
    el.style.top = y + 'px'
    el.style.width = w + 'px'
    el.style.height = h + 'px'
    this.elDiv.appendChild(el)
    let emitArea = GetElementEmitArea(el)
    let pr = GetPreset(preset)
    let particleSystem = new ParticleSystem(pid, emitArea, true, -2, -2, pr.lifeTime, pr.relativeEmitDirection, pr.spawnerInitializer, pr.initializers, pr.systems)
    this.elements[pid] = el
    this.particleSystems[pid] = particleSystem
    return [el, particleSystem]
  }
  AddRadio (x, y, r, pid, preset) {
    console.assert(!this.particleSystems[pid], 'ParticleSystemManager: Particle System already exists, pid: ' + pid)
    let el = document.createElement('input')
    el.id = pid
    el.type = 'radio'
    el.style.position = 'absolute'
    el.style.left = x + 'px'
    el.style.top = y + 'px'
    el.style.width = 2 * r + 'px'
    el.style.height = 2 * r + 'px'
    this.elDiv.appendChild(el)
    let emitArea = GetElementEmitArea(el)
    let pr = GetPreset(preset)
    let particleSystem = new ParticleSystem(pid, emitArea, true, -2, -2, pr.lifeTime, pr.relativeEmitDirection, pr.spawnerInitializer, pr.initializers, pr.systems)
    this.elements[pid] = el
    this.particleSystems[pid] = particleSystem
    return [el, particleSystem]
  }

  InjectSystemsToDOM () {
    let domElements = document.body.querySelectorAll('*')
    let pidIndex = 0
    domElements.forEach((el, i) => {
      let t = (el.type ? el.type.toLowerCase() : '')
      if (t === 'button' || t === 'range' || t === 'number' || t === 'checkbox' || t === 'radio') {
        let emitArea = GetElementEmitArea(el)
        let pr = GetPreset('default')
        let pid = '_' + pidIndex++
        let particleSystem = new ParticleSystem(pid, emitArea, true, -2, -2, pr.lifeTime, pr.relativeEmitDirection, pr.spawnerInitializer, pr.initializers, pr.systems)
        this.elements[pid] = el
        this.particleSystems[pid] = particleSystem
        return [el, particleSystem]
      }
    })
  }

  Remove (pid) {
    let element = this.elements[pid]
    let particleSystem = this.particleSystems[pid]
    console.assert(typeof element !== 'undefined' && typeof particleSystem !== 'undefined', 'Find: element or particle system not found, pid: ' + pid)
    this.elDiv.removeChild(element)
    delete this.elements[pid]
    delete this.particleSystems[pid]
  }

  Find (pid) {
    let element = this.elements[pid]
    let particleSystem = this.particleSystems[pid]
    if (!particleSystem) {
      console.assert(!element, 'Find: element found')
      return [false, false]
    }
    return [ element, particleSystem ]
  }

  GetParticleSystems () {
    return this.particleSystems
  }

  SaveState () {
    let elState = {}
    Object.entries(this.elements).forEach(([key, el]) => {
      elState[key] = {
        id: el.id,
        type: el.type,
        value: el.value,
        clickFlag: el.clickFlag,
        dragFlag: el.dragFlag,
        style: {
          position: el.style.position,
          left: el.style.left,
          top: el.style.top,
          width: el.style.width,
          height: el.style.height
        }
      }
    })
    let replacer = (key, value) => {
      if (key === 'aliveParticles') return undefined
      if (key === 'deadParticles') return undefined
      return value
    }
    let json = JSON.stringify({ elements: elState, particleSystems: this.particleSystems }, replacer, 2)
    return json
  }

  LoadState (json) {
    let state = JSON.parse(json)
    RestoreState(this, state)
  }

  InitFps () {
    let currTime = performance.now()
    this.fpsArray = []
    this.fps = 0
    this.lastFrameTime = currTime
  }

  CalculateFps () {
    let currTime = performance.now()
    let dt = (currTime - this.lastFrameTime) / 1000
    let fps = Math.floor(1 / dt)
    this.fpsArray.push(fps)
    this.lastFrameTime = currTime

    if (this.fpsArray.length >= 60) {
      let sum = this.fpsArray.reduce((a, b) => { return a + b })
      this.fpsAvg = Math.ceil(sum / this.fpsArray.length)
      this.fpsArray = []
    }
  }

  Start () {
    let currTime = Date.now()
    this.canvas.getContext('2d').font = 'normal 12px Arial'
    Object.values(this.particleSystems).forEach(function (system) {
      system.Start(currTime)
    })
  }

  CanvasResizeCheck () {
    if (this.canvas.width !== window.innerWidth) this.canvas.width = window.innerWidth
    if (this.canvas.height !== window.innerHeight) this.canvas.height = window.innerHeight
  }

  Progress () {
    let currTime = Date.now()
    let view = new Rect(0, 0, this.canvas.width, this.canvas.height)
    Object.values(this.particleSystems).forEach(function (system) {
      system.Progress(currTime, view)
    })
  }

  Display () {
    let ctx = this.canvas.getContext('2d')
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    if (this.displayFps) ctx.fillText('FPS: ' + this.fpsAvg, 20, 20)
    Object.values(this.particleSystems).forEach(function (system) {
      system.Display(ctx)
    })
  }

  Run () {
    let requestAnimationFrame = window.requestAnimationFrame
    this.InitFps()
    this.Start()
    let mainLoop = () => {
      this.CalculateFps()
      this.CanvasResizeCheck()
      this.Progress()
      this.Display()
      requestAnimationFrame(mainLoop)
    }
    requestAnimationFrame(mainLoop)
  }
}
export const ParticleSystemManager = new _ParticleSystemManager()
