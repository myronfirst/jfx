import { CSManager } from './CSManager.js'
import { DeepInit } from './Utilities.js'

function InitSpawner (spawnerComponent, spawnerInitializer) {
  DeepInit(spawnerComponent, spawnerInitializer)
  Object.keys(spawnerComponent).forEach((key) => {
    key = Math.floor(key)
  })
}
export class ParticleSystem {
  constructor (_id, _emitArea, _displayEmitArea, _spawnShape, _spawnSide, _lifeTime, _relativeEmitDirection, _spawnerInitializer, _initializers, _systems) {
    this.id = _id
    this.lastTime = 0
    this.aliveParticles = []
    this.deadParticles = []

    this.emitArea = _emitArea
    this.displayEmitArea = _displayEmitArea
    this.spawnShape = _spawnShape
    this.spawnSide = _spawnSide
    this.lifeTime = _lifeTime
    this.relativeEmitDirection = _relativeEmitDirection // 'centerout', 'centerin', 'none'
    this.spawnerComponent = CSManager.GetComponent('spawnerComponent')
    InitSpawner(this.spawnerComponent, _spawnerInitializer)

    this.componentInitializers = {}
    Object.keys(_initializers).forEach((key) => {
      this.componentInitializers[key] = _initializers[key]
    })
    this.systems = {}
    Object.keys(_systems).forEach((key) => {
      this.systems[key] = _systems[key]
    })
  }

  CheckAlive () {
    return (this.aliveParticles.length > 0)
  }

  Start (currTime) {
    this.spawnerComponent.lastTime = currTime
    this.aliveParticles = []
    this.deadParticles = []
  }

  Spawn (currTime) {
    CSManager.GetSystem('spawner').Update(this, currTime)
  }

  CleanDead (currTime, view) {
    for (let i = this.aliveParticles.length - 1; i >= 0; i--) {
      if (!this.aliveParticles[i].CheckAlive(currTime, view)) {
        this.deadParticles.push(this.aliveParticles[i])
        this.aliveParticles.splice(i, 1)
      }
    }
  }

  Progress (currTime, view) {
    this.Spawn(currTime)

    this.aliveParticles.forEach(function (particle, i) {
      particle.Progress(currTime)
    }, this)

    this.CleanDead(currTime, view)
  }

  Display (ctx) {
    if (this.displayEmitArea) this.emitArea.DisplayShapes(ctx)
    this.aliveParticles.forEach(function (particle, i) {
      particle.Display(ctx)
    }, this)
  }
}
