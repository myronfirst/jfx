import { RegisterComponents } from './Components.js'
import { RegisterSpawner } from './Spawner.js'
import { RegisterProgressers } from './Progressers.js'
import { RegisterRenderers } from './Renderers.js'
import { ParticleSystemManager } from './ParticleSystemManager.js'
// import { Clickable, Draggable } from './EventListeners.js'
import { GUI } from './Gui.js'

window.onload = () => {
  RegisterComponents()
  RegisterSpawner()
  RegisterProgressers()
  RegisterRenderers()

  ParticleSystemManager.Initialize()
  ParticleSystemManager.InjectSystemsToDOM()
  // ParticleSystemManager.AddButton(100, 200, 100, 50, 'Button', 'button', 'ringwave')
  // ParticleSystemManager.AddRange(300, 200, 200, 10, 'range', 'rain')
  // ParticleSystemManager.AddNumber(100, 400, 40, 30, 'number', 'rain')
  // ParticleSystemManager.AddCheckbox(300, 400, 16, 16, 'checkbox', 'explosion')
  // ParticleSystemManager.AddRadio(500, 400, 8, 'radio', 'rain')

  // let [ buttonEl, buttonPS ] = ParticleSystemManager.Find('button')
  // let [ checkEl, checkPS ] = ParticleSystemManager.Find('checkbox')
  // buttonPS.spawnerComponent.spawnDelay = -1
  // checkPS.spawnerComponent.spawnDelay = -1
  // Draggable(buttonEl, buttonPS)
  // Clickable(checkEl, checkPS)

  // let json = ParticleSystemManager.SaveState()
  // ParticleSystemManager.LoadState(json)

  GUI.Initialize(ParticleSystemManager)
  GUI.BuildAll()

  ParticleSystemManager.Run()
}
