import { CSManager } from './CSManager.js'

function GetAllComponents (particle) {
  particle.colorComponent = CSManager.GetComponent('colorComponent')
  particle.splatComponent = CSManager.GetComponent('splatComponent')
  particle.scaleComponent = CSManager.GetComponent('scaleComponent')
  particle.rotationComponent = CSManager.GetComponent('rotationComponent')
  particle.velocityComponent = CSManager.GetComponent('velocityComponent')
  particle.ringMotionComponent = CSManager.GetComponent('ringMotionComponent')
  particle.waveMotionComponent = CSManager.GetComponent('waveMotionComponent')
  particle.circleComponent = CSManager.GetComponent('circleComponent')
  particle.rectangleComponent = CSManager.GetComponent('rectangleComponent')
}

export class Particle {
  constructor (_x, _y, _lifeTime, _systems) {
    this.Init(_x, _y, _lifeTime, _systems)
  }

  Init (_x, _y, _lifeTime, _systems) {
    this.position = { x: Math.floor(_x), y: Math.floor(_y) }
    this.lifeTime = Math.floor(_lifeTime)
    this.age = 0
    this.lastTime = 0

    GetAllComponents(this)
    Object.keys(_systems).forEach((systemName) => {
      delete this[systemName]
      if (_systems[systemName]) { this[systemName] = CSManager.GetSystem(systemName) }
    })
  }

  GetNormalizedAge () {
    if (this.lifeTime === 0) return 1.0
    return this.age / this.lifeTime
  }

  GetNormalizedSpeed () {
    let absSpeed = Math.abs(this.velocityComponent.speed)
    if (absSpeed <= this.velocityComponent.start) return 0.0
    if (absSpeed >= this.velocityComponent.end) return 1.0
    return (absSpeed - this.velocityComponent.start) / (this.velocityComponent.end - this.velocityComponent.start)
  }

  CheckAlive (currTime, view) {
    return (this.age < this.lifeTime) && (view.Contains(this.position.x, this.position.y))
  }

  Start (currTime) {
    this.age = 0
    this.lastTime = currTime
  }

  Progress (currTime) {
    let keys = Object.keys(this).filter((key) => {
      return key.includes('Progresser')
    })
    keys.forEach((key, i) => {
      this[key].Update(this, currTime)
    })
    this.age += currTime - this.lastTime
    if (this.age > this.lifeTime) this.age = this.lifeTime
    this.lastTime = currTime
  }

  Display (ctx) {
    let keys = Object.keys(this).filter((key) => {
      return key.includes('Renderer')
    })
    ctx.save()
    keys.forEach((key, i) => {
      this[key].Update(this, ctx)
    })
    ctx.restore()
  }
}
